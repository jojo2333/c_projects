/*******************************************
created by 李沐辰
2016 8.25
******************************************/
#include<stdbool.h>
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>
#define MAX_ACCIDENTS 1000000

#ifndef DATA_STRUCTURE
#define DATA_STRUCTURE

#define BOOL int
#define PROVINCE_NUM 34
#define ACC_TYPE_NUM 13


struct province_info{
    char province_name[20];
    char responsible_pname[15];
    char contact_number[15];
    struct province_info* next_province;
    struct accident_info* accident_header;
};
void pro_init(struct province_info*);

struct accident_info{
    char accident_id[15];
    char occur_time[20];
    char accident_type[12];
    char accident_grade;
    char province_location[20];
    char responsible_company[20];
    int  death_amount;
    int  injured_amount;
    float financial_wastage;
    struct accident_info* next_accident;
    struct report_info* report_header;
};
void acc_init(struct accident_info*);

struct report_info{
    char accident_id[15];
    char report_date[10];
    char media_type;
    char media_name[20];
    char detail_url[80];
    struct report_info* next_report;
};
void report_init(struct report_info*);

//用于储存错误信息
char error_log[50];
char message_log[50];
//用于存储找到的链表头
struct province_info* province_consult_list[40];
struct accident_info* accident_consult_list[MAX_ACCIDENTS];
struct report_info  * report_consult_list[10*MAX_ACCIDENTS];


//传入 指向增加的数据的指针  返回成功 true 失败 false
//存在则更新 不存在则在链尾创建 错误信息保存在 内置搜索 要求前段在数据传入时检查数据完整性
bool update_province(struct province_info*);
bool update_accident(struct accident_info*);
bool update_report(struct report_info*);


//注意要递归的删除指向的整条链//涉及到找出前面一个节点所以要自行实现单链上的搜索
bool delete_province(char *name);
bool delete_accident(char *id);
bool delete_report(char *id,char *acc_name);

//used to accurately locate a info block 全部给出其前一个节点
struct province_info* locate_province(char *name);
struct accident_info* locate_accident(char *id);
struct report_info*   locate_report(char *id,char *med_name);

//used to list all the
//输入： 指向要查询的信息的结构指针
//查询的信息：留空则不加入查询条件
//返回：搜索到的结果数
int search_province(struct province_info*);
int search_accident(struct accident_info*);
int search_report(struct report_info*);

BOOL pro_has_empty(struct province_info *pro);
BOOL acc_has_empty(struct accident_info *acc);
BOOL rpo_has_empty(struct report_info *rpo);
BOOL pro_all_empty(struct province_info *pro);
BOOL acc_all_empty(struct accident_info *acc);
BOOL rpo_all_empty(struct report_info *rpo);

int get_all_pro(void);


//定义哈希表部分
#define TABLESIZE 2000003
struct accident_info *acc_hash_table[TABLESIZE];
struct accident_info *acc_hash_search(char*);
bool acc_hash_insert(struct accident_info*);
unsigned int acc_hash(char *key);
unsigned int acc_hash1(char *key);
bool hashtable_initialize();

#endif // DATA_STRUCTURE
