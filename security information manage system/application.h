/*********************************
createde by 李沐辰
2016 8.26
********************************/
#include<stdbool.h>
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>
#ifndef APPLICATIONS
#define APPLICATIONS
bool readfile(char *filename);
bool savefile(char *filename);
int acc_amount_cmp(const void* ,const void*);
int *accident_yearly_count(char* year);
struct province_info* count_pro_history(void);
struct accident_info* accident_sort_by_report(void);
struct accident_info* accident_count_by_type(void);
struct accident_info* accident_count_by_pro(void);
#endif // APPLICATIONS

