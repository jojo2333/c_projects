/*********************************
createde by 李沐辰
2016 8.26
********************************/
#include "globals.h"

//extern char *ACCIDENT_TYPE[14];
//extern char *PROVINCE_NAME[20];

//char *ACCIDENT_TYPE[14]={"触电","火灾","灼烫","淹溺","高处坠落","坍塌","透水",
//            "火药爆炸","瓦斯爆炸","锅炉爆炸","其他爆炸","中毒和窒息","其他伤害"};
//char *PROVINCE_NAME[20] = {"北京市","天津市","上海市","重庆市","河北省","山西省","辽宁省","吉林省","黑龙江省",
//    "江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","海南省",
//    "四川省","贵州省","云南省","陕西省","甘肃省","青海省","台湾省","内蒙古自治区","广西壮族自治区","西藏自治区",
//    "宁夏回族自治区","新疆维吾尔自治区","香港特别行政区","澳门特别行政区"};

/*
自行规定的一套数据存储结构
某节点信息以‘#’为标志符启动
例如
#province
后面紧跟顺序存储的信息
                                */

bool readfile(char *filename)
{
    FILE *fp = fopen(filename,"r");
    if(fp==NULL)
    {
        strcpy(error_log,"指定文件路径不存在");
        return false;
    }
    char data_buf[50];

    fgets(data_buf,50,fp);
    while(!feof(fp))
    {
        if(data_buf[0]=='\n'){
            fgets(data_buf,50,fp);
            continue;
        }
        else if(data_buf[0]!='#')
        {
            strcpy(error_log,"输入文件格式错误");
            return false;
        }
        data_buf[strlen(data_buf)-1]='\0';
        if(!strcmp(data_buf+1,"province"))
        {
            struct province_info new_pro;
            fscanf(fp,"%s%s%s",new_pro.province_name,new_pro.responsible_pname
                   ,new_pro.contact_number);
            if(update_province(&new_pro)!=true)
                return false;
        }
        else if(!strcmp(data_buf+1,"accident"))
        {
            struct accident_info new_accident;
            fscanf(fp,"%s%s%s",new_accident.accident_id,new_accident.occur_time
                   ,new_accident.accident_type);
            fgetc(fp);
            new_accident.accident_grade = fgetc(fp);
            fscanf(fp,"%s%s%d%d%f",new_accident.province_location,new_accident.responsible_company,
                   &new_accident.death_amount,&new_accident.injured_amount,&new_accident.financial_wastage);
            if(update_accident(&new_accident)!=true)
                return false;
        }
        else if(!strcmp(data_buf+1,"report"))
        {
            struct report_info new_report;
            fscanf(fp,"%s%s",new_report.accident_id,new_report.report_date);
            fgetc(fp);
            new_report.media_type = fgetc(fp);
            fscanf(fp,"%s%s",new_report.media_name,new_report.detail_url);
            if(update_report(&new_report)!=true)
                return false;
        }
        fgets(data_buf,50,fp);
    }
    fclose(fp);
    return true;
}

bool savefile(char *filename)
{
    if(DATABASE_HEADER==NULL)
    {
        strcpy(error_log,"当前数据为空！");
        return false;
    }
    FILE *fp = fopen(filename,"w");

    struct province_info *cur_province;
    struct accident_info *cur_accident;
    struct report_info   *cur_report;


    for(cur_province=DATABASE_HEADER; cur_province!=NULL; cur_province = cur_province->next_province)
    {
        fprintf(fp,"\n#province\n");
        fprintf(fp,"%s\n",cur_province->province_name);
        fprintf(fp,"%s\n",cur_province->responsible_pname);
        fprintf(fp,"%s\n",cur_province->contact_number);

        for(cur_accident=cur_province->accident_header; cur_accident!=NULL;cur_accident=cur_accident->next_accident)
        {
            fprintf(fp,"\n#accident\n");
            fprintf(fp,"%s\n",cur_accident->accident_id);
            fprintf(fp,"%s\n",cur_accident->occur_time);
            fprintf(fp,"%s\n",cur_accident->accident_type);
            fprintf(fp,"%c\n",cur_accident->accident_grade);
            fprintf(fp,"%s\n",cur_accident->province_location);
            fprintf(fp,"%s\n",cur_accident->responsible_company);
            fprintf(fp,"%d\n",cur_accident->death_amount);
            fprintf(fp,"%d\n",cur_accident->injured_amount);
            fprintf(fp,"%f\n",cur_accident->financial_wastage);

            for(cur_report=cur_accident->report_header; cur_report!=NULL; cur_report= cur_report->next_report)
            {
               fprintf(fp,"\n#report\n");
               fprintf(fp,"%s\n",cur_report->accident_id);
               fprintf(fp,"%s\n",cur_report->report_date);
               fprintf(fp,"%c\n",cur_report->media_type);
               fprintf(fp,"%s\n",cur_report->media_name);
               fprintf(fp,"%s\n",cur_report->detail_url);
            }
        }
    }
    fclose(fp);
    return true;
}

struct accident_info *accident_count_by_pro(void)
{
    char *PROVINCE_NAME[34] = {"北京市","天津市","上海市","重庆市","河北省","山西省","辽宁省","吉林省","黑龙江省",
    "江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","海南省",
    "四川省","贵州省","云南省","陕西省","甘肃省","青海省","台湾省","内蒙古自治区","广西壮族自治区","西藏自治区",
    "宁夏回族自治区","新疆维吾尔自治区","香港特别行政区","澳门特别行政区"};

    FILE * fp = fopen("debug.txt","w");
    int i,j;
    unsigned int amount;
    struct accident_info *acc_pnt,*cur,*pnt;
    acc_pnt = (struct accident_info *)malloc(sizeof(struct accident_info)*PROVINCE_NUM);
    for(i=0;i<PROVINCE_NUM;i++)
    {
        cur = acc_pnt+i;
        acc_init(cur);
        strcpy(cur->province_location,PROVINCE_NAME[i]);
        fprintf(fp,"%s %s\n",cur->province_location,PROVINCE_NAME[i]);

        amount = search_accident(cur);
        for(j=0;j<amount;j++)
        {
            pnt = accident_consult_list[j];
            cur->death_amount += pnt->death_amount;
            cur->injured_amount+=pnt->injured_amount;
            cur->financial_wastage+=pnt->financial_wastage;
        }
        cur->next_accident = (struct accident_info*)amount;
    }
    fclose(fp);
    qsort(acc_pnt,PROVINCE_NUM,sizeof(struct accident_info),acc_amount_cmp);
    for(i=0;i<PROVINCE_NUM;i++)
        accident_consult_list[i] = acc_pnt + i;
    return acc_pnt;
}

int *accident_yearly_count(char* year)
{
    int *result;
    result = (int *)malloc(sizeof(int)*4);
    struct accident_info request;
    acc_init(&request);
    strcpy(request.occur_time,year);

    request.accident_grade = '1';       result[0]=search_accident(&request);
    request.accident_grade = '2';       result[1]=search_accident(&request);
    request.accident_grade = '3';       result[2]=search_accident(&request);
    request.accident_grade = '4';       result[3]=search_accident(&request);
    return result;
}

struct accident_info* accident_count_by_type()
{
    char *ACCIDENT_TYPE[13]={"触电","火灾","灼烫","淹溺","高处坠落","坍塌","透水",
            "火药爆炸","瓦斯爆炸","锅炉爆炸","其他爆炸","中毒和窒息","其他伤害"};
    int i,j;
    unsigned int amount;
    struct accident_info request,*list,*pnt;
    acc_init(&request);
    list = (struct accident_info*)malloc(sizeof(struct accident_info)*ACC_TYPE_NUM);
    for(i=0;i<ACC_TYPE_NUM;i++)
    {
        acc_init(list+i);
        strcpy(request.accident_type,ACCIDENT_TYPE[i]);
        strcpy(list[i].accident_type,ACCIDENT_TYPE[i]);
        amount=search_accident(&request);
        for(j=0;j<amount;j++)
        {
            pnt = accident_consult_list[j];
            list[i].death_amount += pnt->death_amount;
            list[i].injured_amount+=pnt->injured_amount;
            list[i].financial_wastage +=pnt->financial_wastage;
        }
        list[i].next_accident = (struct accident_info *)amount;
    }

    qsort(list,ACC_TYPE_NUM,sizeof(struct accident_info),acc_amount_cmp);
    for(i=0;i<ACC_TYPE_NUM;i++)
        accident_consult_list[i] = list + i;
    return list;
}

struct accident_info* accident_sort_by_report()
{
    struct province_info *pro_pnt;
    struct accident_info *acc_pnt,*acc_sort_list;
    struct report_info   *repo_pnt;
    acc_sort_list = (struct accident_info*)malloc(sizeof(struct accident_info)*10);

    int i=0,j,pos;
    for(pro_pnt=DATABASE_HEADER;pro_pnt!=NULL;pro_pnt=pro_pnt->next_province)
        for(acc_pnt=pro_pnt->accident_header;acc_pnt!=NULL;acc_pnt = acc_pnt->next_accident)
        {
            unsigned int amount = 0;
            for(repo_pnt=acc_pnt->report_header;repo_pnt!=NULL;repo_pnt=repo_pnt->next_report)
                amount++;
            if(i<10){
                acc_sort_list[i]=*acc_pnt;
                acc_sort_list[i].next_accident = (struct accident_info*)amount;
                i++;
            }
            else if(i==10)
                qsort(acc_sort_list,10,sizeof(struct accident_info),acc_amount_cmp);
            else{
                for(pos=10;pos>=0;pos--)
                {
                    if(amount<(unsigned int)acc_sort_list[i].next_accident)
                        break;
                }
                if(pos<10)
                {
                    for(j=9;j>pos;j--)
                        acc_sort_list[j]=acc_sort_list[j-1];
                }
                acc_sort_list[pos] = *acc_pnt;
                acc_sort_list[pos].next_accident = (struct accident_info*)amount;
            }
        }
    for(i=0;i<10;i++)
        accident_consult_list[i] = acc_sort_list + i;
    return acc_sort_list;
}


int acc_amount_cmp(const void* a,const void* b)
{
    int a_amount = (unsigned int)((struct accident_info*)a)->next_accident;
    int b_amount = (unsigned int)((struct accident_info*)b)->next_accident;
    return b_amount-a_amount;
}
