#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <wincon.h>
#include <conio.h>
#include <string.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <ctype.h>
#include <time.h>
#include <locale.h>
#include "data_structure.h"
#include "application.h"

#ifndef TYPE_H_INCLUDED
#define TYPE_H_INCLUDED

#define SCR_ROW 25             /*屏幕行数*/
#define SCR_COL 80             /*屏幕列数*/

#define FRO_W_BAC_LBLUE BACKGROUND_BLUE|BACKGROUND_INTENSITY|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED
#define FRO_BLACK_BAC_W BACKGROUND_BLUE|BACKGROUND_GREEN|BACKGROUND_RED
#define FOR_W_BAC_BLUE BACKGROUND_BLUE|BACKGROUND_INTENSITY|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY
#define FOR_LW BACKGROUND_BLUE|BACKGROUND_GREEN|BACKGROUND_RED|BACKGROUND_INTENSITY
#define BOOL int
#define PAGESIZE 12
#define INDEX_POS SCR_ROW-4
/**
 *屏幕窗口信息链结点结点结构
 */
typedef struct layer_node {
    char LayerNo;            /**< 弹出窗口层数*/
    SMALL_RECT rcArea;       /**< 弹出窗口区域坐标*/
    CHAR_INFO *pContent;     /**< 弹出窗口区域字符单元原信息存储缓冲区*/
    char *pScrAtt;           /**< 弹出窗口区域字符单元原属性值存储缓冲区*/
    struct layer_node *next; /**< 指向下一结点的指针*/
} LAYER_NODE;

/**
 *标签束结构
 */
typedef struct labe1_bundle {
    char **ppLabel;        /**< 标签字符串数组首地址*/
    COORD *pLoc;           /**< 标签定位数组首地址*/
    int num;               /**< 标签个数*/
} LABEL_BUNDLE;

/**
 *热区结构
 */
typedef struct hot_area {
    SMALL_RECT *pArea;     /**< 热区定位数组首地址*/
    char *pSort;           /**< 热区类别(按键、文本框、选项框)数组首地址*/
    char *pTag;            /**< 热区序号数组首地址*/
    int num;               /**< 热区个数*/
} HOT_AREA;

struct out_put_list_info{
    int total;
    int page;
    void (*pnt)(int,int);
}INFO_LIST;



void clean_input_buf(void);
void set_background(SMALL_RECT *area,WORD att);
void Show_message(char *pnt);
void InitInterface(void);                 /*系统界面初始化*/
void ClearScreen(void);                         /*清屏*/
void ShowMenu(void);                            /*显示菜单栏*/
void PopMenu(int num);                          /*显示下拉菜单*/
void PopPrompt(int num);                        /*显示弹出窗口*/
void PopUp(SMALL_RECT *, WORD, LABEL_BUNDLE *, HOT_AREA *);  /*弹出窗口屏幕信息维护*/
void PopOff(void);                              /*关闭顶层弹出窗口*/
void DrawBox(SMALL_RECT *parea);                /*绘制边框*/
void LocSubMenu(int num, SMALL_RECT *parea);    /*主菜单下拉菜单定位*/
void ShowState(char *);                           /*显示状态栏*/
void TagMainMenu(int num);                      /*标记被选中的主菜单项*/
void TagSubMenu(int num);                       /*标记被选中的子菜单项*/
int DealInput(HOT_AREA *pHotArea, int *piHot,SMALL_RECT *box);
int DealConInput(HOT_AREA *phot_area, int *pihot_num);  /*控制台输入处理*/
void SetHotPoint(HOT_AREA *phot_area, int hot_num);     /*设置热区*/
void RunSys(void);                  /*系统功能模块的选择和运行*/
BOOL ExeFunction(int main_menu_num, int sub_menu_num);  /*功能模块的调用*/
void CloseSys(void);                  /*退出系统*/
BOOL ShowModule(char **pString, int n,int *type,int input_length);

BOOL LoadData(void);           /*数据加载*/
BOOL SaveData(void);           /*保存数据*/
BOOL BackupData(void);         /*备份数据*/
BOOL RestoreData(void);        /*恢复数据*/
BOOL ExitSys(void);            /*退出系统*/
BOOL HelpTopic(void);          /*帮助主体*/
BOOL AboutDorm(void);          /*关于系统*/

BOOL ProvinceDataManage(void); /*维护省份信息*/
BOOL AccidentDataManage(void); /*维护事件信息*/
BOOL ReportDataManage(void);   /*维护报道信息*/

BOOL ProvinceDataSearch(void); /*查询省份信息*/
BOOL AccidentDataSearch(void); /*查询事故信息*/
BOOL ReportDataSearch(void);   /*查询报道信息*/

BOOL StatisticsView(void);
BOOL AdvancedStatistic(void);

void WtoProvince(struct province_info *pro);
void WtoAccident(struct accident_info *acc);
void WtoReport(struct report_info *rpo);
void WtoAccident_S(struct accident_info *acc);
void WtoReport_S(struct report_info *rpo);
void WtoReport_E(struct report_info *rpo);

void ProvincetoW(struct province_info* pro);
void AccidenttoW(struct accident_info* acc);
void ReporttoW(struct report_info* rpo);

void Show_province(int total,int page);
void Show_accident(int total,int page);
void Show_report(int total,int page);

void Show_accident_count_by_pro(int total,int page);
void Show_accident_count_by_type(int total,int page);
void Show_accident_sort_by_report(int total,int page);

void set_index(void);
void deal_index(COORD pos);

#endif /**< TYPE_H_INCLUDED*/
