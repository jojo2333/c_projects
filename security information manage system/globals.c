#include "ui.h"
#include "data_structure.h"
#include "application.h"
struct province_info* DATABASE_HEADER;
LAYER_NODE *gp_top_layer = NULL;               /*弹出窗口信息链链头*/


int ga_sub_menu_count[] = {5, 4, 4, 2, 3};  /*各主菜单项下子菜单的个数*/
int ga_sub_real_menu_count[] = {5,3,3,2,2};
int gi_sel_menu = 1;                        /*被选中的主菜单项号,初始为1*/
int gi_sel_sub_menu = 0;                    /*被选中的子菜单项号,初始为0,表示未选中*/
WCHAR Input[9][50];
int  input_pointer[9]={0,0,0,0,0,0,0,0,0};

CHAR_INFO *gp_buff_menubar_info = NULL;     /*存放菜单条屏幕区字符信息的缓冲区*/
CHAR_INFO *gp_buff_stateBar_info = NULL;    /*存放状态条屏幕区字符信息的缓冲区*/

char *gp_scr_att = NULL;    /*存放屏幕上字符单元属性值的缓冲区*/
char *gp_sex_code = NULL;   /*存放性别代码表的数据缓冲区*/
char *gp_type_code = NULL;  /*存放学生类别代码表的数据缓冲区*/
char gc_sys_state = '\0';   /*用来保存系统状态的字符*/

unsigned long gul_sex_code_len = 0;    /*性别代码表长度*/
unsigned long gul_type_code_len = 0;   /*学生类别代码表长度*/

HANDLE gh_std_out;          /*标准输出设备句柄*/
HANDLE gh_std_in;           /*标准输入设备句柄*/

//char *ACCIDENT_TYPE[13]={"触电","火灾","灼烫","淹溺","高处坠落","坍塌","透水",
//            "火药爆炸","瓦斯爆炸","锅炉爆炸","其他爆炸","中毒和窒息","其他伤害"};
//char *PROVINCE_NAME[34] = {"北京市","天津市","上海市","重庆市","河北省","山西省","辽宁省","吉林省","黑龙江省",
//    "江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","海南省",
//    "四川省","贵州省","云南省","陕西省","甘肃省","青海省","台湾省","内蒙古自治区","广西壮族自治区","西藏自治区",
//    "宁夏回族自治区","新疆维吾尔自治区","香港特别行政区","澳门特别行政区"};

int index_pos[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

struct accident_info *P_accident_count_by_pro = NULL;
struct accident_info *P_accident_count_by_type = NULL;
struct accident_info *P_accident_sort_by_report=NULL;
