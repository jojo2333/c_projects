/*********************************
createde by ְמדו³½
2016 8.26
********************************/
#include"data_structure.h"
//初始化哈希表
bool hashtable_initialize()
{
    int i;
    for(i=0;i<=TABLESIZE;i++)
        acc_hash_table[i] = NULL;
    return true;
}

//哈希插入
bool acc_hash_insert(struct accident_info *acc)
{
    char *id = acc->accident_id;

    unsigned int hash_pos = acc_hash(id);
    unsigned int hash_add = acc_hash1(id);
    int i=0;
    while(acc_hash_table[hash_pos]!=NULL&&i<TABLESIZE)
    {
        i++;
        hash_pos = (hash_pos+hash_add)%TABLESIZE;
    }
    if(i>=TABLESIZE)
        return false;
    acc_hash_table[hash_pos] = acc;
    return true;
}

//哈希查找
struct accident_info *acc_hash_search(char *id)
{
    unsigned int hash_pos = acc_hash(id);
    unsigned int hash_add = acc_hash1(id);
    int i = 0;
    while(acc_hash_table[hash_pos]!=NULL&&i<TABLESIZE)
    {
        i++;
        if(!strcmp(acc_hash_table[hash_pos]->accident_id,id))
            return acc_hash_table[hash_pos];

        hash_pos = (hash_pos+hash_add)%TABLESIZE;

    }
    return NULL;
}


//采用双散列方法进行hash插入
unsigned int acc_hash1(char *key)
{
    unsigned int Hashval1=0;
    while(*key!='\0')
        Hashval1 +=(Hashval1*10 +*key++)%(TABLESIZE-2);

    return Hashval1;
}

unsigned int acc_hash(char *key)
{
    unsigned int Hashval = 0;

    while(*key!='\0')
        Hashval = (Hashval<<3) + *key++;

    return Hashval % TABLESIZE;
}


