#include "globals.h"


void Show_message(char *pnt)
{
    char **plabel_name;
    plabel_name = (char **)malloc(sizeof(char *)*2);
    plabel_name[0] = (char *)malloc(sizeof(char)*30);
    plabel_name[1] = (char *)malloc(sizeof(char)*30);
    strcpy(plabel_name[0],pnt);
    strcpy(plabel_name[1],"确认");
    int type[2] = {-1,0};
    int input_length = 0;
    ShowModule(plabel_name,2,type,input_length);
    free(plabel_name[0]);   free(plabel_name[1]);
    free(plabel_name);
}


void Show_province(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_province;
    ClearScreen();
    COORD cursor_pos,pos = {4,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("编号    省份名称         省份负责人      联系电话        ");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct province_info **p = province_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-8d%-17s%-16s%-16s",i+1,p[i]->province_name,p[i]->responsible_pname,p[i]->contact_number);
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
    }
    set_index();
}

void Show_accident(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_accident;
    ClearScreen();
    COORD cursor_pos,pos = {3,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("事故编号    发生时间        事故类型   等级 发生单位   死亡 重伤 经济损失");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,73,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct accident_info **p = accident_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-12s%-16s%-11s%-5c%-11s%-5d%-5d%.2f",p[i]->accident_id,p[i]->occur_time,p[i]->accident_type,
               p[i]->accident_grade,p[i]->responsible_company,p[i]->death_amount,
               p[i]->injured_amount,p[i]->financial_wastage);
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,73,cursor_pos,&len);
    }
    set_index();
}

void Show_report(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_report;
    ClearScreen();
    COORD cursor_pos,pos = {4,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("媒体名称      事故编号    报道日期   媒体类别 内容索引");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct report_info **p = report_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-14s%-12s%-11s%-9c%s",p[i]->media_name,p[i]->accident_id,p[i]->report_date,
               p[i]->media_type,p[i]->detail_url);
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
    }
    set_index();
}


void Show_accident_count_by_pro(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_accident_count_by_pro;
    ClearScreen();
    COORD cursor_pos,pos = {4,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("编号    省份           事故总数  死亡人数  重伤人数  直接经济损失");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct accident_info **p = accident_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-8d%-15s%-10d%-10d%-10d%f",i+1,p[i]->province_location,(unsigned int)(p[i]->next_accident),
               p[i]->death_amount,p[i]->injured_amount,p[i]->financial_wastage);
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
    }
    set_index();
}

void Show_accident_count_by_type(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_accident_count_by_type;
    ClearScreen();
    COORD cursor_pos,pos = {4,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("编号    事故类别    事故总数  死亡人数  重伤人数  直接经济损失");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct accident_info **p = accident_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-8d%-12s%-10d%-10d%-10d%f",i+1,p[i]->accident_type,(unsigned int)(p[i]->next_accident),
               p[i]->death_amount,p[i]->injured_amount,p[i]->financial_wastage);
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
    }
    set_index();
}

void Show_accident_sort_by_report(int total,int page)
{
    INFO_LIST.page = page;
    INFO_LIST.total=total;
    INFO_LIST.pnt = Show_accident_sort_by_report;
    ClearScreen();
    COORD cursor_pos,pos = {4,3};
    //int sub_pos[4] = {0,6,23,39};
    int i,begin,end;
    DWORD len;

    SetConsoleCursorPosition(gh_std_out,pos);
    printf("序号 事故编号    事故类型    事故等级    所属省份    媒体报道次数");
    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
    begin = (page-1)*PAGESIZE;
    end   = begin + PAGESIZE -1;
    if(end > total-1) end = total-1;
    for(i=begin;i<=end ;i++)
    {
        struct accident_info **p = accident_consult_list;
        cursor_pos.Y = pos.Y+2+i-begin;
        cursor_pos.X = pos.X;
        SetConsoleCursorPosition(gh_std_out,cursor_pos);
        printf("%-5d%-12s%-12s%-12c%-12s%d",i+1,p[i]->accident_id,p[i]->accident_type,
               p[i]->accident_grade,p[i]->province_location,(unsigned int)(p[i]->next_accident));
        FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
    }
    set_index();
}

void set_index(void)
{
    int total_pages = (INFO_LIST.total-1)/PAGESIZE + 1;
//    int current_page= INFO_LIST.page;
//    void (*func)(int,int) =INFO_LIST.pnt;
    int i;

    if(INFO_LIST.total==0)
        return;
    DWORD len;
    COORD cursor_pos,pos = {4,21};
    SetConsoleCursorPosition(gh_std_out,pos);
    total_pages = total_pages>15?15:total_pages;

    for(i=0;i<total_pages;i++){
        printf(" %d ",i+1);
        if(i==0)
            index_pos[i] = pos.X;
        else if(i<9)
            index_pos[i] = index_pos[i-1]+3;
        else
            index_pos[i] = index_pos[i-1]+4;
    }
    if(i<9)
        index_pos[i] = index_pos[i-1]+3;
    else
        index_pos[i] = index_pos[i-1]+4;

    printf("    front      next  ");

    FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,SCR_COL-6,pos,&len);

//    cursor_pos = pos;
//    for(i=0;i<total_pages;i++){
//        WORD size = index_pos[i+1]-index_pos[i];
//        cursor_pos.X = pos.X + index_pos[i];
//        FillConsoleOutputAttribute(gh_std_out,FRO_W_BAC_LBLUE,size,cursor_pos,&len);
//    }
//
//    cursor_pos.X = index_pos[total_pages] + 2;
//    FillConsoleOutputAttribute(gh_std_out,FRO_W_BAC_LBLUE,9,cursor_pos,&len);
//    cursor_pos.X = index_pos[total_pages] + 13;
//    FillConsoleOutputAttribute(gh_std_out,FRO_W_BAC_LBLUE,8,cursor_pos,&len);
}

void deal_index(COORD pos)
{
    int total_pages = (INFO_LIST.total-1)/PAGESIZE + 1;
    int current_page= INFO_LIST.page;
    void (*func)(int,int) =INFO_LIST.pnt;
    int i;

    if(INFO_LIST.total<=0)  return;

    for(i=0;i<total_pages;i++)
    {
        if(pos.X>index_pos[i] && pos.X<index_pos[i+1]){
            current_page = i+1;
            break;
        }
    }
    if(i>=total_pages)
    {
        if(pos.X>index_pos[total_pages]+2 && pos.X<index_pos[total_pages]+11)
            current_page--;
        else if(pos.X>index_pos[total_pages]+13 && pos.X<index_pos[total_pages]+21)
            current_page++;
        if(current_page<=0||current_page>total_pages)
            return;
    }
    func(INFO_LIST.total,current_page);
}
