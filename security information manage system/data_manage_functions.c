/*********************************************
created by 李沐辰
2016 8.26
********************************************/
#include "globals.h"

void pro_init(struct province_info *pro)
{
    pro->province_name[0]='\0';
    pro->responsible_pname[0]='\0';
    pro->contact_number[0]='\0';
    pro->accident_header=NULL;
    pro->next_province = NULL;
}

void acc_init(struct accident_info *acc)
{
    acc->accident_grade = '\0';
    acc->accident_id[0] = '\0';
    acc->accident_type[0]='\0';
    acc->death_amount    = 0 ;
    acc->occur_time[0]   ='\0';
    acc->financial_wastage=0;
    acc->injured_amount   =0;
    acc->province_location[0]='\0';
    acc->responsible_company[0]='\0';
    acc->next_accident   = NULL;
    acc->report_header   = NULL;
}

void report_init(struct report_info* repo)
{
    repo->accident_id[0] = '\0';
    repo->detail_url[0]  = '\0';
    repo->media_name[0]  = '\0';
    repo->media_type     = '\0';
    repo->report_date[0] = '\0';
    repo->next_report    = NULL;
}

//更新省份信息 若相应省份存在则覆盖 不存在则新建
bool update_province(struct province_info* p_province)
{
    struct province_info* des_province=NULL;
    des_province = locate_province(p_province->province_name);

    //该省份已存在
    if(des_province!=NULL){
        p_province->next_province = des_province->next_province;
        p_province->accident_header=des_province->accident_header;

        *des_province = *p_province;
        strcpy(message_log,"省份信息修改成功!");
    }
    //相应的省份信息不存在
    else{
        des_province = (struct province_info*)malloc(sizeof(struct province_info));

        *des_province=*p_province;
        des_province->next_province = DATABASE_HEADER;
        des_province->accident_header = NULL;
        DATABASE_HEADER = des_province;
        strcpy(message_log,"省份信息添加成功！");
    }
    return true;
}

//更新事件信息 若相应事件存在则覆盖 不存在则新建
bool update_accident(struct accident_info* p_accident)
{
    struct accident_info* des_accident=NULL;
    des_accident = locate_accident(p_accident->accident_id);

    //该事故信息不存在
    if(des_accident!=NULL){
        p_accident->next_accident = des_accident->next_accident;
        p_accident->report_header = des_accident->report_header;

        *des_accident = *p_accident;
        strcpy(message_log,"安全事故信息修改成功!");
    }
    //若该事故信息不存在 新建 前提是所属省份已经导入
    else{
        struct province_info* parent_province = NULL;
        parent_province = locate_province(p_accident->province_location);

        if(parent_province!=NULL){
            des_accident  = (struct accident_info*)malloc(sizeof(struct accident_info));

            *des_accident = *p_accident;
            des_accident->next_accident = parent_province->accident_header;
            des_accident->report_header = NULL;
            parent_province->accident_header = des_accident;

            bool status=acc_hash_insert(des_accident);
            assert(status=true);

            strcpy(message_log,"安全事故信息添加成功!");
        }
        else{
            strcpy(error_log,"所属省份不存在！请先添加省份信息。");
            return false;
        }
    }
    return true;
}

//更新报道信息 若相应报道存在则覆盖 不存在则新建
bool update_report(struct report_info* p_report)
{
    struct report_info* des_report=NULL;
    des_report = locate_report(p_report->accident_id,p_report->media_name);

    //若已存在则覆盖
    if(des_report!=NULL){
        p_report->next_report = des_report->next_report;

        *des_report = *p_report;
        strcpy(message_log,"媒体报告信息修改成功");
    }
    else{
        struct accident_info* parent_accident = NULL;
        parent_accident = locate_accident(p_report->accident_id);

        if(parent_accident!=NULL){
            des_report = (struct report_info*)malloc(sizeof(struct report_info));

            *des_report = *p_report;
            des_report->next_report = parent_accident->report_header;
            parent_accident->report_header = des_report;
            strcpy(message_log,"媒体报告信息添加成功");
        }
        else{
            strcpy(error_log,"所属事故不存在！请先添加事故信息。");
            return false;
        }
    }
    return true;
}

//递归删除省份及其附属事故、报道链表
bool delete_province(char *pro_name)
{
    struct province_info *front_province = NULL,*this_province = NULL;
    struct accident_info *this_accident = NULL,*del_accident;
    this_province = locate_province(pro_name);
    if(this_province==NULL){
        strcpy(error_log,"指定删除省份不存在！");
        return false;
    }
    this_accident = this_province->accident_header;

    while(this_accident!=NULL)
    {
        del_accident = this_accident;
        delete_accident(del_accident->accident_id);
        this_accident = this_accident->next_accident;
    }

    //判断 删除的如果是 数据库头直接指向的节点 和非直接指向的节点要分开处理
    if(!strcmp(DATABASE_HEADER->province_name,pro_name)){
        DATABASE_HEADER = this_province->next_province;
    }
    else{
        front_province = DATABASE_HEADER;
        this_province  = DATABASE_HEADER->next_province;
        while(this_province!=NULL)
        {
            if(!strcmp(this_province->province_name,pro_name))
                break;
            front_province = this_province;
            this_province = this_province->next_province;
        }
        assert(this_province!=NULL);

        front_province->next_province = this_province->next_province;
    }
    free(this_province);
    strcpy(message_log,"删除一个省份与相关信息成功！");
    return true;
}

//递归删除事件及附属报道链表
bool delete_accident(char *acc_id)
{
    struct province_info *parent_province = NULL;
    struct accident_info *front_accident = NULL,*this_accident = NULL;
    struct report_info   *this_report = NULL,*del_report = NULL;

    this_accident   = locate_accident(acc_id);
    if(this_accident==NULL){
        strcpy(error_log,"指定删除事故不存在！");
        return false;
    }
    parent_province = locate_province(this_accident->province_location);
    this_report = this_accident->report_header;

    while(this_report!=NULL)
    {
        del_report = this_report;
        delete_report(del_report->accident_id,del_report->media_name);
        this_report = this_report->next_report;
    }

    //判断 删除的如果是 省份直接指向的节点 和非直接指向的节点要分开处理
    if(!strcmp(parent_province->accident_header->accident_id,acc_id))
    {
        parent_province->accident_header = this_accident->next_accident;
    }
    else{
        front_accident = parent_province->accident_header;
        this_accident  = front_accident->next_accident;

        while(this_accident!=NULL)
        {
            if(!strcmp(this_accident->accident_id,acc_id))
                break;
            front_accident = this_accident;
            this_accident = this_accident->next_accident;
        }
        assert(this_accident!=NULL);

        front_accident->next_accident = this_accident->next_accident;
    }
    free(this_accident);
    strcpy(message_log,"删除一个安全事故与相关信息成功！");
    return true;
}

//删除报道节点
bool delete_report(char *acc_id,char* med_name)
{
    struct accident_info *parent_accident = NULL;
    struct report_info   *front_report = NULL,*this_report = NULL;

    parent_accident = locate_accident(acc_id);

    //判断 删除的如果是 事件直接指向的节点 和非直接指向的节点要分开处理
    if(!strcmp(parent_accident->report_header->media_name,med_name))
    {
        this_report = parent_accident->report_header;
        parent_accident->report_header = this_report->next_report;
    }
    else{
        front_report = parent_accident->report_header;
        this_report  = front_report->next_report;

        while(this_report!=NULL)
        {
            if(!strcmp(this_report->media_name,med_name))
                break;
            front_report = this_report;
            this_report = this_report->next_report;
        }
        assert(this_report!=NULL);

        front_report->next_report = this_report->next_report;
    }
    free(this_report);
    strcpy(message_log,"删除一个媒体报道信息成功！");
    return true;
}

//从数据库链头开始顺序查找
struct province_info* locate_province(char* pro_name)
{
    struct province_info* this_province = DATABASE_HEADER;

    while(this_province!=NULL)
    {
        if(!strcmp(this_province->province_name,pro_name))
            return this_province;
        this_province = this_province->next_province;
    }
    return NULL;
}

//1.locate_accident 多次用到2.整个信息存储系统中 事故信息是连接上下的主体 用hash进行优化
struct accident_info* locate_accident(char* acc_id)
{
    return acc_hash_search(acc_id);
}

//report 的locate 是基于已有的 accident_id 进行顺序查找后得到的
struct report_info* locate_report(char *acc_id,char *med_name)
{
    struct accident_info *parent_accident = NULL;
    struct report_info   *this_report = NULL;
    parent_accident = locate_accident(acc_id);

    if(parent_accident==NULL){
        return NULL;
    }
    this_report = parent_accident->report_header;

    while(this_report!=NULL)
    {
        if(!strcmp(this_report->media_name,med_name))
            return this_report;
        this_report = this_report->next_report;
    }
    return NULL;
}

//顺序查找省份
int search_province(struct province_info* request)
{
    struct province_info *p_province;
    int amount = 0;
    if(pro_all_empty(request))
        return 0;
    //如果省份名字不为空 则直接locate出省份位置
    if(request->province_name[0]!='\0'){
        province_consult_list[0] = locate_province(request->province_name);
        if(province_consult_list[0]==NULL)
            return 0;
        else
            return 1;
    }

    //否则从头指针开始顺序查找相关省份信息
    for(p_province = DATABASE_HEADER;p_province!=NULL;p_province = p_province->next_province)
    {
        if(strcmp(request->responsible_pname,p_province->responsible_pname)!=0
           && request->responsible_pname[0]!='\0')                     continue;
        //如果该域不为空且不相等则说明当前节点不符合查找条件 跳过到下一个节点
        if(strcmp(request->contact_number,p_province->contact_number)!=0
           && request->contact_number[0]!='\0')                        continue;

        province_consult_list[amount++] = p_province;
    }
    return amount;
}

//顺序查找符合条件的事件
int search_accident(struct accident_info* request)
{
    struct accident_info *p_accident;
    struct province_info *cur_province;
    int amount = 0;

    if(acc_all_empty(request))
        return 0;
    if(request->accident_id[0] != '\0'){
        accident_consult_list[0] = locate_accident(request->accident_id);
        if(accident_consult_list[0]==NULL)
            return 0;
        else
            return 1;
    }

    for(cur_province=DATABASE_HEADER; cur_province!=NULL; cur_province=cur_province->next_province)
    {
        for(p_accident=cur_province->accident_header; p_accident!=NULL; p_accident=p_accident->next_accident)
        {
            //如果省份信息不合直接跳过此省
            if(strcmp(p_accident->province_location,request->province_location)!=0
               && request->province_location[0]!='\0')                         break;
            if(p_accident->accident_grade != request->accident_grade
               && request->accident_grade!= '\0' )                            continue;
            if(strcmp(p_accident->accident_type,request->accident_type)!=0
               && request->accident_type[0]!=  '\0')                             continue;
            if(request->occur_time[0]   ==  '\0');
            else{
                char t[15];
                strcpy(t,p_accident->occur_time);
                t[4] = '\0';
                if(strcmp(request->occur_time,t)!=0)
                    continue;
            }
            if(strcmp(p_accident->responsible_company,request->responsible_company)!=0
               && request->responsible_company[0] != '\0')                       continue;

            accident_consult_list[amount++] = p_accident;
        }
    }
    return amount;
}

//顺序查找符合条件的报告
int search_report(struct report_info* request)
{
    struct report_info *p_report;
    struct accident_info *cur_accident;
    struct province_info *cur_province;
    int amount = 0;

    if(rpo_all_empty(request))
        return 0;
    //如果accident_id域不为空 则直接找到该事件对应的报道链
    if(request->accident_id[0]!= '\0')
    {
        cur_accident = locate_accident(request->accident_id);
        for(p_report=cur_accident->report_header; p_report!=NULL; p_report= p_report->next_report)
        {
           if(strstr(p_report->media_name,request->media_name)!=NULL
            && request->media_name[0] != '\0')               continue;
           if(p_report->media_type != request->media_type
            && request->media_type != '\0')               continue;
           if(strcmp(p_report->report_date,request->report_date)!=0
            && request->report_date[0]!= '\0')              continue;

            report_consult_list[amount++] = p_report;
        }
        return amount;
    }
    //否则顺序查找符合条件的报道信息
    for(cur_province=DATABASE_HEADER; cur_province!=NULL; cur_province = cur_province->next_province)
    {
        for(cur_accident=cur_province->accident_header; cur_accident!=NULL;cur_accident=cur_accident->next_accident)
        {
            for(p_report=cur_accident->report_header; p_report!=NULL; p_report= p_report->next_report)
            {
               if(strstr(p_report->media_name,request->media_name)!=NULL
                && request->media_name[0] != '\0')               continue;
               if(p_report->media_type != request->media_type
                && request->media_type != 0)               continue;
               if(strcmp(p_report->report_date,request->report_date)!=0
                && request->report_date[0]!= '\0')              continue;

               report_consult_list[amount++] = p_report;
            }
        }
    }
    return amount;
}

BOOL pro_has_empty(struct province_info *pro)
{
    if(!strcmp(pro->province_name,"")||!strcmp(pro->responsible_pname,"")
       ||!strcmp(pro->contact_number,""))
        return 1;
    else
        return 0;
}

BOOL acc_has_empty(struct accident_info *acc)
{
    if(!strcmp(acc->accident_id,"")||!strcmp(acc->accident_type,"")||
       !strcmp(acc->occur_time,"")||!strcmp(acc->province_location,"")||
       !strcmp(acc->responsible_company,"")||acc->accident_grade==0||
       acc->death_amount==0||acc->injured_amount==0||acc->financial_wastage==0)
        return 1;
    else
        return 0;
}

BOOL rpo_has_empty(struct report_info *rpo)
{
    if(!strcmp(rpo->accident_id,"")||!strcmp(rpo->report_date,"")||
       !strcmp(rpo->media_name,"")||!strcmp(rpo->detail_url,"")||
       rpo->media_type ==0)
        return 1;
    else
        return 0;
}

BOOL pro_all_empty(struct province_info *pro)
{
    if(!strcmp(pro->province_name,"")&&!strcmp(pro->responsible_pname,"")
       &&!strcmp(pro->contact_number,""))
        return 1;
    else
        return 0;
}

BOOL acc_all_empty(struct accident_info *acc)
{
    if(!strcmp(acc->accident_id,"")&&!strcmp(acc->accident_type,"")&&
       !strcmp(acc->occur_time,"")&&!strcmp(acc->province_location,"")&&
       !strcmp(acc->responsible_company,"")&&acc->accident_grade==0&&
       acc->death_amount==0&&acc->injured_amount==0&&acc->financial_wastage==0)
        return 1;
    else
        return 0;
}

BOOL rpo_all_empty(struct report_info *rpo)
{
    if(!strcmp(rpo->accident_id,"")&&!strcmp(rpo->report_date,"")&&
       !strcmp(rpo->media_name,"")&&!strcmp(rpo->detail_url,"")&&
       rpo->media_type ==0)
        return 1;
    else
        return 0;
}

int get_all_pro(void)
{
    struct province_info *p = DATABASE_HEADER;
    int i=0;

    while(p!=NULL){
        province_consult_list[i++]=p;
        p = p->next_province;
    }
    return i;
}
