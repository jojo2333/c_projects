#include "ui.h"
#include "data_structure.h"
#include "application.h"

extern struct province_info* DATABASE_HEADER;

extern LAYER_NODE *gp_top_layer;               /*弹出窗口信息链链头*/


extern int ga_sub_menu_count[5];  /*各主菜单项下子菜单的个数*/
extern int ga_sub_real_menu_count[5];
extern int gi_sel_menu;                        /*被选中的主菜单项号,初始为1*/
extern int gi_sel_sub_menu;                    /*被选中的子菜单项号,初始为0,表示未选中*/
extern WCHAR Input[9][40];
extern int  input_pointer[9];

extern CHAR_INFO *gp_buff_menubar_info;     /*存放菜单条屏幕区字符信息的缓冲区*/
extern CHAR_INFO *gp_buff_stateBar_info;    /*存放状态条屏幕区字符信息的缓冲区*/

extern char *gp_scr_att;    /*存放屏幕上字符单元属性值的缓冲区*/
extern char *gp_sex_code;   /*存放性别代码表的数据缓冲区*/
extern char *gp_type_code;  /*存放学生类别代码表的数据缓冲区*/
extern char gc_sys_state;   /*用来保存系统状态的字符*/

extern unsigned long gul_sex_code_len;    /*性别代码表长度*/
extern unsigned long gul_type_code_len;   /*学生类别代码表长度*/

extern HANDLE gh_std_out;          /*标准输出设备句柄*/
extern HANDLE gh_std_in;           /*标准输入设备句柄*/
extern int index_pos[16];

extern struct accident_info *P_accident_count_by_pro;
extern struct accident_info *P_accident_count_by_type;
extern struct accident_info *P_accident_sort_by_report;


