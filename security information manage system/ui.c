#include "globals.h"
unsigned long ul;
char *gp_sys_name = "灾害事故信息管理系统";    /*系统名称*/

char *ga_main_menu[] = {"文件(F)",             /*系统主菜单名*/
    	                "数据维护(M)",
                        "数据查询(Q)",
                        "数据统计(S)",
                        "帮助(H)"
                       };

char *ga_sub_menu[] = {"[L] 读取文件",          /*系统子菜单名*/
                       "[S] 数据保存",
                       "[B] 数据备份",
                       "[R] 数据恢复",
                       "[X] 退出    Alt+X",
                       "[S] 省份信息",
                       "[T] 事件信息",
                       "[D] 报道信息",           /*空串用来在弹出菜单中分隔子菜单项，下同*/
                       "",
                       "[S] 省份信息查询",
                       "[T] 事故信息查询",
                       "[D] 事故报道信息查询",
                       "",
                       "[C] 统计",
                       "[F] 自定义统计",
                       "[T] 帮助主题",
                       "",
                       "[A] 关于..."
                      };

int ui()
{
    setlocale( LC_ALL, "chs" );
    COORD size = {SCR_COL, SCR_ROW};              /*窗口缓冲区大小*/

    gh_std_out = GetStdHandle(STD_OUTPUT_HANDLE); /* 获取标准输出设备句柄*/
    gh_std_in = GetStdHandle(STD_INPUT_HANDLE);   /* 获取标准输入设备句柄*/

//    SMALL_RECT ws = {0,0,SCR_COL,SCR_ROW};
//    SetConsoleWindowInfo(gh_std_out,TRUE,&ws);
    SetConsoleTitle(gp_sys_name);                 /*设置窗口标题*/
    SetConsoleScreenBufferSize(gh_std_out, size); /*设置窗口缓冲区大小80*25*/

    InitInterface();          /*界面初始化*/
    RunSys();             /*系统功能模块的选择及运行*/
    CloseSys();            /*退出系统*/

    return 0;
}

/**
 * 函数名称: InitInterface
 * 函数功能: 初始化界面.
 * 输入参数: 无
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void InitInterface()
{
    WORD att = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY
               | BACKGROUND_BLUE;  /*黄色前景和蓝色背景*/

    SetConsoleTextAttribute(gh_std_out, att);  /*设置控制台屏幕缓冲区字符属性*/

    ClearScreen();  /* 清屏*/

    /*创建弹出窗口信息堆栈，将初始化后的屏幕窗口当作第一层弹出窗口*/
    gp_scr_att = (char *)calloc(SCR_COL * SCR_ROW, sizeof(char));/*屏幕字符属性*/
    gp_top_layer = (LAYER_NODE *)malloc(sizeof(LAYER_NODE));
    gp_top_layer->LayerNo = 0;      /*弹出窗口的层号为0*/
    gp_top_layer->rcArea.Left = 0;  /*弹出窗口的区域为整个屏幕窗口*/
    gp_top_layer->rcArea.Top = 0;
    gp_top_layer->rcArea.Right = SCR_COL - 1;
    gp_top_layer->rcArea.Bottom = SCR_ROW - 1;
    gp_top_layer->pContent = NULL;
    gp_top_layer->pScrAtt = gp_scr_att;
    gp_top_layer->next = NULL;

    ShowMenu();        /*显示菜单栏*/
    ShowState("");     /*显示状态栏*/

    Show_province(get_all_pro(),1);

    return;
}

/**
 * 函数名称: ClearScreen
 * 函数功能: 清除屏幕信息.
 * 输入参数: 无
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void ClearScreen(void)
{
    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    COORD home = {0, 1};
    unsigned long size;

    GetConsoleScreenBufferInfo( gh_std_out, &bInfo );/*取屏幕缓冲区信息*/
    size =  bInfo.dwSize.X * (bInfo.dwSize.Y-2); /*计算屏幕缓冲区字符单元数*/

    /*将屏幕缓冲区所有单元的字符属性设置为当前屏幕缓冲区字符属性*/
    FillConsoleOutputAttribute(gh_std_out, bInfo.wAttributes, size, home, &ul);

    /*将屏幕缓冲区所有单元填充为空格字符*/
    FillConsoleOutputCharacter(gh_std_out, ' ', size, home, &ul);


    CHAR_INFO  *lpBuffer;
    int i;

    COORD pos = {0,0};
    COORD wsize = {SCR_COL-2,SCR_ROW-4};
    SMALL_RECT area = {1,2,SCR_COL-2,SCR_ROW-3};
    lpBuffer = (CHAR_INFO*)malloc(wsize.X*wsize.Y*sizeof(CHAR_INFO));
    for(i=0;i<wsize.X*wsize.Y;i++)
    {
        if( i==0 || i==wsize.X-1 || i==wsize.X*(wsize.Y-1) || i==wsize.X*wsize.Y-1)
            lpBuffer[i].Char.AsciiChar = '+';
        else if(i/wsize.X == 0 || i/wsize.X == wsize.Y-1)
            lpBuffer[i].Char.AsciiChar = '-';
        else if(i%wsize.X == 0 || i%wsize.X == wsize.X-1)
            lpBuffer[i].Char.AsciiChar = '|';
        else
            lpBuffer[i].Char.AsciiChar = ' ';
        lpBuffer[i].Attributes= BACKGROUND_BLUE | BACKGROUND_GREEN| BACKGROUND_RED ;
    }
    WriteConsoleOutput(gh_std_out,lpBuffer,wsize,pos,&area);
    free(lpBuffer);
    return;
}

/**
 * 函数名称: ShowMenu
 * 函数功能: 在屏幕上显示主菜单, 并设置热区, 在主菜单第一项上置选中标记.
 * 输入参数: 无
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void ShowMenu()
{
    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    CONSOLE_CURSOR_INFO lpCur;
    COORD size;
    COORD pos = {0, 0};
    int i, j;
    int PosA = 2, PosB;
    char ch;

    GetConsoleScreenBufferInfo( gh_std_out, &bInfo );
    size.X = bInfo.dwSize.X;
    size.Y = 1;
    SetConsoleCursorPosition(gh_std_out, pos);
    for (i=0; i < 5; i++) /*在窗口第一行第一列处输出主菜单项*/
    {
        printf("  %s  ", ga_main_menu[i]);
    }

    GetConsoleCursorInfo(gh_std_out, &lpCur);
    lpCur.bVisible = FALSE;
    SetConsoleCursorInfo(gh_std_out, &lpCur);  /*隐藏光标*/

    /*申请动态存储区作为存放菜单条屏幕区字符信息的缓冲区*/
    gp_buff_menubar_info = (CHAR_INFO *)malloc(size.X * size.Y * sizeof(CHAR_INFO));
    SMALL_RECT rcMenu ={0, 0, size.X-1, 0} ;

    /*将窗口第一行的内容读入到存放菜单条屏幕区字符信息的缓冲区中*/
    ReadConsoleOutput(gh_std_out, gp_buff_menubar_info, size, pos, &rcMenu);

    /*将这一行中英文字母置为红色，其他字符单元置为白底黑字*/
    for (i=0; i<size.X; i++)
    {
        (gp_buff_menubar_info+i)->Attributes = BACKGROUND_BLUE | BACKGROUND_GREEN
                                               | BACKGROUND_RED;
        ch = (char)((gp_buff_menubar_info+i)->Char.AsciiChar);
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            (gp_buff_menubar_info+i)->Attributes |= FOREGROUND_RED;
        }
    }

    /*修改后的菜单条字符信息回写到窗口的第一行*/
    WriteConsoleOutput(gh_std_out, gp_buff_menubar_info, size, pos, &rcMenu);
    COORD endPos = {0, 1};
    SetConsoleCursorPosition(gh_std_out, endPos);  /*将光标位置设置在第2行第1列*/

    /*将菜单项置为热区，热区编号为菜单项号，热区类型为0(按钮型)*/
    i = 0;
    do
    {
        PosB = PosA + strlen(ga_main_menu[i]);  /*定位第i+1号菜单项的起止位置*/
        for (j=PosA; j<PosB; j++)
        {
            gp_scr_att[j] |= (i+1) << 2; /*设置菜单项所在字符单元的属性值*/
        }
        PosA = PosB + 4;
        i++;
    } while (i<5);

    TagMainMenu(gi_sel_menu);  /*在选中主菜单项上做标记，gi_sel_menu初值为1*/

    return;
}

/**
 * 函数名称: ShowState
 * 函数功能: 显示状态条.
 * 输入参数: 无
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明: 状态条字符属性为白底黑字, 初始状态无状态信息.
 */
void ShowState(char *message)
{
    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    COORD size;
    COORD pos = {0, 0};
    COORD cur_pos;
    int i;
    char ch;

    GetConsoleScreenBufferInfo( gh_std_out, &bInfo );
    size.X = bInfo.dwSize.X;
    size.Y = 1;
    cur_pos.X = 0;  cur_pos.Y = bInfo.dwSize.Y-1;
    SMALL_RECT rcMenu ={0, bInfo.dwSize.Y-1, size.X-1, bInfo.dwSize.Y-1};

    if (gp_buff_stateBar_info == NULL)
        gp_buff_stateBar_info = (CHAR_INFO *)malloc(size.X * size.Y * sizeof(CHAR_INFO));

    SetConsoleCursorPosition(gh_std_out,cur_pos);
    printf("%-40s",message);
    ReadConsoleOutput(gh_std_out, gp_buff_stateBar_info, size, pos, &rcMenu);

    for (i=0; i<size.X; i++)
    {
        (gp_buff_stateBar_info+i)->Attributes = BACKGROUND_BLUE | BACKGROUND_GREEN
                                                | BACKGROUND_RED;

        ch = (char)((gp_buff_stateBar_info+i)->Char.AsciiChar);
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            (gp_buff_stateBar_info+i)->Attributes |= FOREGROUND_RED;
        }
    }

    WriteConsoleOutput(gh_std_out, gp_buff_stateBar_info, size, pos, &rcMenu);
    SetConsoleCursorPosition(gh_std_out,cur_pos);
    return;
}

/**
 * 函数名称: TagMainMenu
 * 函数功能: 在指定主菜单项上置选中标志.
 * 输入参数: num 选中的主菜单项号
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void TagMainMenu(int num)
{
    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    COORD size;
    COORD pos = {0, 0};
    int PosA = 2, PosB;
    char ch;
    int i;

    if (num == 0) /*num为0时，将会去除主菜单项选中标记*/
    {
        PosA = 0;
        PosB = 0;
    }
    else  /*否则，定位选中主菜单项的起止位置: PosA为起始位置, PosB为截止位置*/
    {
        for (i=1; i<num; i++)
        {
            PosA += strlen(ga_main_menu[i-1]) + 4;
        }
        PosB = PosA + strlen(ga_main_menu[num-1]);
    }

    GetConsoleScreenBufferInfo( gh_std_out, &bInfo );
    size.X = bInfo.dwSize.X;
    size.Y = 1;

    /*去除选中菜单项前面的菜单项选中标记*/
    for (i=0; i<PosA; i++)
    {
        (gp_buff_menubar_info+i)->Attributes = BACKGROUND_BLUE | BACKGROUND_GREEN
                                               | BACKGROUND_RED;
        ch = (gp_buff_menubar_info+i)->Char.AsciiChar;
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            (gp_buff_menubar_info+i)->Attributes |= FOREGROUND_RED;
        }
    }

    /*在选中菜单项上做标记，黑底白字*/
    for (i=PosA; i<PosB; i++)
    {
        (gp_buff_menubar_info+i)->Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN
                                               | FOREGROUND_RED;
    }

    /*去除选中菜单项后面的菜单项选中标记*/
    for (i=PosB; i<bInfo.dwSize.X; i++)
    {
        (gp_buff_menubar_info+i)->Attributes = BACKGROUND_BLUE | BACKGROUND_GREEN
                                               | BACKGROUND_RED;
        ch = (char)((gp_buff_menubar_info+i)->Char.AsciiChar);
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            (gp_buff_menubar_info+i)->Attributes |= FOREGROUND_RED;
        }
    }

    /*将做好标记的菜单条信息写到窗口第一行*/
    SMALL_RECT rcMenu ={0, 0, size.X-1, 0};
    WriteConsoleOutput(gh_std_out, gp_buff_menubar_info, size, pos, &rcMenu);

    return;
}

/**
 * 函数名称: CloseSys
 * 函数功能: 关闭系统.
 * 输入参数: hd 主链头指针
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void CloseSys(void)
{


    ClearScreen();        /*清屏*/

    /*释放存放菜单条、状态条、性别代码和学生类别代码等信息动态存储区*/
    free(gp_buff_menubar_info);
    free(gp_buff_stateBar_info);
    free(gp_sex_code);
    free(gp_type_code);

    /*关闭标准输入和输出设备句柄*/
    CloseHandle(gh_std_out);
    CloseHandle(gh_std_in);

    /*将窗口标题栏置为运行结束*/
    SetConsoleTitle("运行结束");

    return;
}

/**
 * 函数名称: RunSys
 * 函数功能: 运行系统, 在系统主界面下运行用户所选择的功能模块.
 * 输入参数: 无
 * 输出参数: phead 主链头指针的地址
 * 返 回 值: 无
 *
 * 调用说明:
 */
void RunSys(void)
{
    CONSOLE_CURSOR_INFO lpCur;
    INPUT_RECORD inRec;
    DWORD res;
    COORD pos = {0, 0};
    BOOL bRet = TRUE;
    int i, loc, num;
    int cNo, cAtt;      /*cNo:字符单元层号, cAtt:字符单元属性*/
    char vkc, asc;      /*vkc:虚拟键代码, asc:字符的ASCII码值*/

    while (bRet)
    {
        clean_input_buf();
        GetConsoleCursorInfo(gh_std_out, &lpCur);
        lpCur.bVisible = FALSE;
        SetConsoleCursorInfo(gh_std_out, &lpCur);
        /*从控制台输入缓冲区中读一条记录*/
        ReadConsoleInput(gh_std_in, &inRec, 1, &res);

        if (inRec.EventType == MOUSE_EVENT) /*如果记录由鼠标事件产生*/
        {
            pos = inRec.Event.MouseEvent.dwMousePosition;  /*获取鼠标坐标位置*/
            cNo = gp_scr_att[pos.Y * SCR_COL + pos.X] & 3; /*取该位置的层号*/
            cAtt = gp_scr_att[pos.Y * SCR_COL + pos.X] >> 2;/*取该字符单元属性*/
            if (cNo == 0) /*层号为0，表明该位置未被弹出子菜单覆盖*/
            {
                /* cAtt > 0 表明该位置处于热区(主菜单项字符单元)
                 * cAtt != gi_sel_menu 表明该位置的主菜单项未被选中
                 * gp_top_layer->LayerNo > 0 表明当前有子菜单弹出
                 */
                if (cAtt > 0 && cAtt != gi_sel_menu && gp_top_layer->LayerNo > 0)
                {
                    PopOff();            /*关闭弹出的子菜单*/
                    gi_sel_sub_menu = 0; /*将选中子菜单项的项号置为0*/
                    PopMenu(cAtt);       /*弹出鼠标所在主菜单项对应的子菜单*/
                }
            }
            else if (cAtt > 0) /*鼠标所在位置为弹出子菜单的菜单项字符单元*/
            {
                TagSubMenu(cAtt); /*在该子菜单项上做选中标记*/
            }

            if (inRec.Event.MouseEvent.dwButtonState
                == FROM_LEFT_1ST_BUTTON_PRESSED) /*如果按下鼠标左边第一键*/
            {
                if (cNo == 0) /*层号为0，表明该位置未被弹出子菜单覆盖*/
                {
                    if (cAtt > 0) /*如果该位置处于热区(主菜单项字符单元)*/
                    {
                        PopMenu(cAtt);   /*弹出鼠标所在主菜单项对应的子菜单*/
                    }
                    /*如果该位置不属于主菜单项字符单元，且有子菜单弹出*/
                    else if (gp_top_layer->LayerNo > 0)
                    {
                        PopOff();            /*关闭弹出的子菜单*/
                        gi_sel_sub_menu = 0; /*将选中子菜单项的项号置为0*/
                    }
                    else if(pos.Y == INDEX_POS)
                    {
                        deal_index(pos);
                    }
                }
                else /*层号不为0，表明该位置被弹出子菜单覆盖*/
                {
                    if (cAtt > 0) /*如果该位置处于热区(子菜单项字符单元)*/
                    {
                        PopOff(); /*关闭弹出的子菜单*/
                        gi_sel_sub_menu = 0; /*将选中子菜单项的项号置为0*/

                        /*执行对应功能函数:gi_sel_menu主菜单项号,cAtt子菜单项号*/
                        bRet = ExeFunction(gi_sel_menu, cAtt);
                    }
                }
            }
            else if (inRec.Event.MouseEvent.dwButtonState
                     == RIGHTMOST_BUTTON_PRESSED) /*如果按下鼠标右键*/
            {
                if (cNo == 0) /*层号为0，表明该位置未被弹出子菜单覆盖*/
                {
                    PopOff();            /*关闭弹出的子菜单*/
                    gi_sel_sub_menu = 0; /*将选中子菜单项的项号置为0*/
                }
            }
        }
        else if (inRec.EventType == KEY_EVENT  /*如果记录由按键产生*/
                 && inRec.Event.KeyEvent.bKeyDown) /*且键被按下*/
        {
            vkc = inRec.Event.KeyEvent.wVirtualKeyCode; /*获取按键的虚拟键码*/
            asc = inRec.Event.KeyEvent.uChar.AsciiChar; /*获取按键的ASC码*/

            /*系统快捷键的处理*/
            if (vkc == 112) /*如果按下F1键*/
            {
                if (gp_top_layer->LayerNo != 0) /*如果当前有子菜单弹出*/
                {
                    PopOff();            /*关闭弹出的子菜单*/
                    gi_sel_sub_menu = 0; /*将选中子菜单项的项号置为0*/
                }
                bRet = ExeFunction(5, 1);  /*运行帮助主题功能函数*/
            }
            else if (inRec.Event.KeyEvent.dwControlKeyState
                     & (LEFT_ALT_PRESSED | RIGHT_ALT_PRESSED))
            { /*如果按下左或右Alt键*/
                switch (vkc)  /*判断组合键Alt+字母*/
                {
                    case 88:  /*Alt+X 退出*/
                        if (gp_top_layer->LayerNo != 0)
                        {
                            PopOff();
                            gi_sel_sub_menu = 0;
                        }
                        bRet = ExeFunction(1,4);
                        break;
                    case 70:  /*Alt+F*/
                        PopMenu(1);
                        break;
                    case 77: /*Alt+M*/
                        PopMenu(2);
                        break;
                    case 81: /*Alt+Q*/
                        PopMenu(3);
                        break;
                    case 83: /*Alt+S*/
                        PopMenu(4);
                        break;
                    case 72: /*Alt+H*/
                        PopMenu(5);
                        break;
                }
            }
            else if (asc == 0) /*其他控制键的处理*/
            {
                if (gp_top_layer->LayerNo == 0) /*如果未弹出子菜单*/
                {
                    switch (vkc) /*处理方向键(左、右、下)，不响应其他控制键*/
                    {
                        case 37:
                            gi_sel_menu--;
                            if (gi_sel_menu == 0)
                            {
                                gi_sel_menu = 5;
                            }
                            TagMainMenu(gi_sel_menu);
                            break;
                        case 39:
                            gi_sel_menu++;
                            if (gi_sel_menu == 6)
                            {
                                gi_sel_menu = 1;
                            }
                            TagMainMenu(gi_sel_menu);
                            break;
                        case 40:
                            PopMenu(gi_sel_menu);
                            TagSubMenu(1);
                            break;
                    }
                }
                else  /*已弹出子菜单时*/
                {
                    for (loc=0,i=1; i<gi_sel_menu; i++)
                    {
                        loc += ga_sub_menu_count[i-1];
                    }  /*计算该子菜单中的第一项在子菜单字符串数组中的位置(下标)*/
                    switch (vkc) /*方向键(左、右、上、下)的处理*/
                    {
                        case 37:
                            gi_sel_menu--;
                            if (gi_sel_menu < 1)
                            {
                                gi_sel_menu = 5;
                            }
                            TagMainMenu(gi_sel_menu);
                            PopOff();
                            PopMenu(gi_sel_menu);
                            TagSubMenu(1);
                            break;
                        case 38:
                            num = gi_sel_sub_menu - 1;
                            if (num < 1)
                            {
                                num = ga_sub_menu_count[gi_sel_menu-1];
                            }
                            if (strlen(ga_sub_menu[loc+num-1]) == 0)
                            {
                                num--;
                            }
                            TagSubMenu(num);
                            break;
                        case 39:
                            gi_sel_menu++;
                            if (gi_sel_menu > 5)
                            {
                                gi_sel_menu = 1;
                            }
                            TagMainMenu(gi_sel_menu);
                            PopOff();
                            PopMenu(gi_sel_menu);
                            TagSubMenu(1);
                            break;
                        case 40:
                            num = gi_sel_sub_menu + 1;
                            if (num > ga_sub_menu_count[gi_sel_menu-1])
                            {
                                num = 1;
                            }
                            if (strlen(ga_sub_menu[loc+num-1]) == 0)
                            {
                                num++;
                            }
                            TagSubMenu(num);
                            break;
                    }
                }
            }
            else if ((asc-vkc == 0) || (asc-vkc == 32)){  /*按下普通键*/
                if (gp_top_layer->LayerNo == 0)  /*如果未弹出子菜单*/
                {
                    switch (vkc)
                    {
                        case 70: /*f或F*/
                            PopMenu(1);
                            break;
                        case 77: /*m或M*/
                            PopMenu(2);
                            break;
                        case 81: /*q或Q*/
                            PopMenu(3);
                            break;
                        case 83: /*s或S*/
                            PopMenu(4);
                            break;
                        case 72: /*h或H*/
                            PopMenu(5);
                            break;
                        case 13: /*回车*/
                            PopMenu(gi_sel_menu);
                            TagSubMenu(1);
                            break;
                    }
                }
                else /*已弹出子菜单时的键盘输入处理*/
                {
                    if (vkc == 27) /*如果按下ESC键*/
                    {
                        PopOff();
                        gi_sel_sub_menu = 0;
                    }
                    else if(vkc == 13) /*如果按下回车键*/
                    {
                        num = gi_sel_sub_menu;
                        PopOff();
                        gi_sel_sub_menu = 0;
                        bRet = ExeFunction(gi_sel_menu, num);
                    }
                    else /*其他普通键的处理*/
                    {
                        /*计算该子菜单中的第一项在子菜单字符串数组中的位置(下标)*/
                        for (loc=0,i=1; i<gi_sel_menu; i++)
                        {
                            loc += ga_sub_menu_count[i-1];
                        }

                        /*依次与当前子菜单中每一项的代表字符进行比较*/
                        for (i=loc; i<loc+ga_sub_menu_count[gi_sel_menu-1]; i++)
                        {
                            if (strlen(ga_sub_menu[i])>0 && vkc==ga_sub_menu[i][1])
                            { /*如果匹配成功*/
                                PopOff();
                                gi_sel_sub_menu = 0;
                                bRet = ExeFunction(gi_sel_menu, i-loc+1);
                            }
                        }
                    }
                }
            }
        }
    }
}

void PopPrompt(int num)
{

}

/**
 * 函数名称: PopMenu
 * 函数功能: 弹出指定主菜单项对应的子菜单.
 * 输入参数: num 指定的主菜单项号
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void PopMenu(int num)
{
    LABEL_BUNDLE labels;
    HOT_AREA areas;
    SMALL_RECT rcPop;
    COORD pos;
    WORD att;
    char *pCh;
    int i, j, loc = 0;

    if (num != gi_sel_menu)       /*如果指定主菜单不是已选中菜单*/
    {
        if (gp_top_layer->LayerNo != 0) /*如果此前已有子菜单弹出*/
        {
            PopOff();
            gi_sel_sub_menu = 0;
        }
    }
    else if (gp_top_layer->LayerNo != 0) /*若已弹出该子菜单，则返回*/
    {
        return;
    }

    gi_sel_menu = num;    /*将选中主菜单项置为指定的主菜单项*/
    TagMainMenu(gi_sel_menu); /*在选中的主菜单项上做标记*/
    LocSubMenu(gi_sel_menu, &rcPop); /*计算弹出子菜单的区域位置, 存放在rcPop中*/

    /*计算该子菜单中的第一项在子菜单字符串数组中的位置(下标)*/
    for (i=1; i<gi_sel_menu; i++)
    {
        loc += ga_sub_menu_count[i-1];
    }
    /*将该组子菜单项项名存入标签束结构变量*/
    labels.ppLabel = ga_sub_menu + loc;   /*标签束第一个标签字符串的地址*/
    labels.num = ga_sub_menu_count[gi_sel_menu-1]; /*标签束中标签字符串的个数*/
    COORD aLoc[labels.num];/*定义一个坐标数组，存放每个标签字符串输出位置的坐标*/
    for (i=0; i<labels.num; i++) /*确定标签字符串的输出位置，存放在坐标数组中*/
    {
        aLoc[i].X = rcPop.Left + 2;
        aLoc[i].Y = rcPop.Top + i + 1;
    }
    labels.pLoc = aLoc; /*使标签束结构变量labels的成员pLoc指向坐标数组的首元素*/
    /*设置热区信息*/
    areas.num = labels.num;       /*热区的个数，等于标签的个数，即子菜单的项数*/
    SMALL_RECT aArea[areas.num];                    /*定义数组存放所有热区位置*/
    char aSort[areas.num];                      /*定义数组存放所有热区对应类别*/
    char aTag[areas.num];                         /*定义数组存放每个热区的编号*/
    for (i=0; i<areas.num; i++)
    {
        aArea[i].Left = rcPop.Left + 2;  /*热区定位*/
        aArea[i].Top = rcPop.Top + i + 1;
        aArea[i].Right = rcPop.Right - 2;
        aArea[i].Bottom = aArea[i].Top;
        aSort[i] = 0;       /*热区类别都为0(按钮型)*/
        aTag[i] = i + 1;           /*热区按顺序编号*/
    }
    areas.pArea = aArea;/*使热区结构变量areas的成员pArea指向热区位置数组首元素*/
    areas.pSort = aSort;/*使热区结构变量areas的成员pSort指向热区类别数组首元素*/
    areas.pTag = aTag;   /*使热区结构变量areas的成员pTag指向热区编号数组首元素*/

    att = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;  /*白底黑字*/
    PopUp(&rcPop, att, &labels, &areas);
    DrawBox(&rcPop);  /*给弹出窗口画边框*/
    pos.X = rcPop.Left + 2;
    for (pos.Y=rcPop.Top+1; pos.Y<rcPop.Bottom; pos.Y++)
    { /*此循环用来在空串子菜项位置画线形成分隔，并取消此菜单项的热区属性*/
        pCh = ga_sub_menu[loc+pos.Y-rcPop.Top-1];
        if (strlen(pCh)==0) /*串长为0，表明为空串*/
        {   /*首先画横线*/
            FillConsoleOutputCharacter(gh_std_out, '-', rcPop.Right-rcPop.Left-3, pos, &ul);
            for (j=rcPop.Left+2; j<rcPop.Right-1; j++)
            {   /*取消该区域字符单元的热区属性*/
                gp_scr_att[pos.Y*SCR_COL+j] &= 3; /*按位与的结果保留了低两位*/
            }
        }

    }
    /*将子菜单项的功能键设为白底红字*/
    pos.X = rcPop.Left + 3;
    att =  FOREGROUND_RED | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;
    for (pos.Y=rcPop.Top+1; pos.Y<rcPop.Bottom; pos.Y++)
    {
        if (strlen(ga_sub_menu[loc+pos.Y-rcPop.Top-1])==0)
        {
            continue;  /*跳过空串*/
        }
        FillConsoleOutputAttribute(gh_std_out, att, 1, pos, &ul);
    }
    return;
}

/**
 * 函数名称: PopUp
 * 函数功能: 在指定区域输出弹出窗口信息, 同时设置热区, 将弹出窗口位置信息入栈.
 * 输入参数: pRc 弹出窗口位置数据存放的地址
 *           att 弹出窗口区域字符属性
 *           pLabel 弹出窗口中标签束信息存放的地址
             pHotArea 弹出窗口中热区信息存放的地址
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void PopUp(SMALL_RECT *pRc, WORD att, LABEL_BUNDLE *pLabel, HOT_AREA *pHotArea)
{
    LAYER_NODE *nextLayer;
    COORD size;
    COORD pos = {0, 0};
    char *pCh;
    int i, j, row;

    /*弹出窗口所在位置字符单元信息入栈*/
    size.X = pRc->Right - pRc->Left + 1;    /*弹出窗口的宽度*/
    size.Y = pRc->Bottom - pRc->Top + 1;    /*弹出窗口的高度*/
    /*申请存放弹出窗口相关信息的动态存储区*/
    nextLayer = (LAYER_NODE *)malloc(sizeof(LAYER_NODE));
    nextLayer->next = gp_top_layer;
    nextLayer->LayerNo = gp_top_layer->LayerNo + 1;
    nextLayer->rcArea = *pRc;
    nextLayer->pContent = (CHAR_INFO *)malloc(size.X*size.Y*sizeof(CHAR_INFO));
    nextLayer->pScrAtt = (char *)malloc(size.X*size.Y*sizeof(char));
    pCh = nextLayer->pScrAtt;
    /*将弹出窗口覆盖区域的字符信息保存，用于在关闭弹出窗口时恢复原样*/
    ReadConsoleOutput(gh_std_out, nextLayer->pContent, size, pos, pRc);
    for (i=pRc->Top; i<=pRc->Bottom; i++)
    {   /*此二重循环将所覆盖字符单元的原先属性值存入动态存储区，便于以后恢复*/
        for (j=pRc->Left; j<=pRc->Right; j++)
        {
            *pCh = gp_scr_att[i*SCR_COL+j];
            pCh++;
        }
    }
    gp_top_layer = nextLayer;  /*完成弹出窗口相关信息入栈操作*/
    /*设置弹出窗口区域字符的新属性*/
    pos.X = pRc->Left;
    pos.Y = pRc->Top;
    for (i=pRc->Top; i<=pRc->Bottom; i++)
    {
        FillConsoleOutputAttribute(gh_std_out, att, size.X, pos, &ul);
        pos.Y++;
    }
    set_background(pRc,att);
    /*将标签束中的标签字符串在设定的位置输出*/
    for (i=0; i<pLabel->num; i++)
    {
        pCh = pLabel->ppLabel[i];
        if (strlen(pCh) != 0)
        {
            WriteConsoleOutputCharacter(gh_std_out, pCh, strlen(pCh),
                                        pLabel->pLoc[i], &ul);
        }
    }
    /*设置弹出窗口区域字符单元的新属性*/
    for (i=pRc->Top; i<=pRc->Bottom; i++)
    {   /*此二重循环设置字符单元的层号*/
        for (j=pRc->Left; j<=pRc->Right; j++)
        {
            gp_scr_att[i*SCR_COL+j] = gp_top_layer->LayerNo;
        }
    }

    for (i=0; i<pHotArea->num; i++)
    {   /*此二重循环设置所有热区中字符单元的热区类型和热区编号*/
        row = pHotArea->pArea[i].Top;
        for (j=pHotArea->pArea[i].Left; j<=pHotArea->pArea[i].Right; j++)
        {
            gp_scr_att[row*SCR_COL+j] |= (pHotArea->pSort[i] << 6)
                                    | (pHotArea->pTag[i] << 2);
        }
    }
    return;
}

/**
 * 函数名称: PopOff
 * 函数功能: 关闭顶层弹出窗口, 恢复覆盖区域原外观和字符单元原属性.
 * 输入参数: 无
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void PopOff(void)
{
    LAYER_NODE *nextLayer;
    COORD size;
    COORD pos = {0, 0};
    char *pCh;
    int i, j;

    if ((gp_top_layer->next==NULL) || (gp_top_layer->pContent==NULL))
    {   /*栈底存放的主界面屏幕信息，不用关闭*/
        return;
    }
    nextLayer = gp_top_layer->next;
    /*恢复弹出窗口区域原外观*/
    size.X = gp_top_layer->rcArea.Right - gp_top_layer->rcArea.Left + 1;
    size.Y = gp_top_layer->rcArea.Bottom - gp_top_layer->rcArea.Top + 1;
    WriteConsoleOutput(gh_std_out, gp_top_layer->pContent, size, pos, &(gp_top_layer->rcArea));
    /*恢复字符单元原属性*/
    pCh = gp_top_layer->pScrAtt;
    for (i=gp_top_layer->rcArea.Top; i<=gp_top_layer->rcArea.Bottom; i++)
    {
        for (j=gp_top_layer->rcArea.Left; j<=gp_top_layer->rcArea.Right; j++)
        {
            gp_scr_att[i*SCR_COL+j] = *pCh;
            pCh++;
        }
    }
    free(gp_top_layer->pContent);    /*释放动态存储区*/
    free(gp_top_layer->pScrAtt);
    free(gp_top_layer);
    gp_top_layer = nextLayer;
    gi_sel_sub_menu = 0;
    return;
}

/**
 * 函数名称: DrawBox
 * 函数功能: 在指定区域画边框.
 * 输入参数: pRc 存放区域位置信息的地址
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void DrawBox(SMALL_RECT *pRc)
{
    char chBox[] = {'+','-','|'};  /*画框用的字符*/
    COORD pos = {pRc->Left, pRc->Top};  /*定位在区域的左上角*/

    WriteConsoleOutputCharacter(gh_std_out, &chBox[0], 1, pos, &ul);/*画边框左上角*/
    for (pos.X = pRc->Left + 1; pos.X < pRc->Right; pos.X++)
    {   /*此循环画上边框横线*/
        WriteConsoleOutputCharacter(gh_std_out, &chBox[1], 1, pos, &ul);
    }
    pos.X = pRc->Right;
    WriteConsoleOutputCharacter(gh_std_out, &chBox[0], 1, pos, &ul);/*画边框右上角*/
    for (pos.Y = pRc->Top+1; pos.Y < pRc->Bottom; pos.Y++)
    {   /*此循环画边框左边线和右边线*/
        pos.X = pRc->Left;
        WriteConsoleOutputCharacter(gh_std_out, &chBox[2], 1, pos, &ul);
        pos.X = pRc->Right;
        WriteConsoleOutputCharacter(gh_std_out, &chBox[2], 1, pos, &ul);
    }
    pos.X = pRc->Left;
    pos.Y = pRc->Bottom;
    WriteConsoleOutputCharacter(gh_std_out, &chBox[0], 1, pos, &ul);/*画边框左下角*/
    for (pos.X = pRc->Left + 1; pos.X < pRc->Right; pos.X++)
    {   /*画下边框横线*/
        WriteConsoleOutputCharacter(gh_std_out, &chBox[1], 1, pos, &ul);
    }
    pos.X = pRc->Right;
    WriteConsoleOutputCharacter(gh_std_out, &chBox[0], 1, pos, &ul);/*画边框右下角*/
    return;
}

/**
 * 函数名称: TagSubMenu
 * 函数功能: 在指定子菜单项上做选中标记.
 * 输入参数: num 选中的子菜单项号
 * 输出参数: 无
 * 返 回 值: 无
 *
 * 调用说明:
 */
void TagSubMenu(int num)
{
    SMALL_RECT rcPop;
    COORD pos;
    WORD att;
    int width;

    LocSubMenu(gi_sel_menu, &rcPop);  /*计算弹出子菜单的区域位置, 存放在rcPop中*/
    if ((num<1) || (num == gi_sel_sub_menu) || (num>rcPop.Bottom-rcPop.Top-1))
    {   /*如果子菜单项号越界，或该项子菜单已被选中，则返回*/
        return;
    }

    pos.X = rcPop.Left + 2;
    width = rcPop.Right - rcPop.Left - 3;
    if (gi_sel_sub_menu != 0) /*首先取消原选中子菜单项上的标记*/
    {
        pos.Y = rcPop.Top + gi_sel_sub_menu;
        att = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;  /*白底黑字*/
        FillConsoleOutputAttribute(gh_std_out, att, width, pos, &ul);
        pos.X += 1;
        att |=  FOREGROUND_RED;/*白底红字*/
        FillConsoleOutputAttribute(gh_std_out, att, 1, pos, &ul);
    }
    /*在制定子菜单项上做选中标记*/
    pos.X = rcPop.Left + 2;
    pos.Y = rcPop.Top + num;
    att = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;  /*黑底白字*/
    FillConsoleOutputAttribute(gh_std_out, att, width, pos, &ul);
    gi_sel_sub_menu = num;  /*修改选中子菜单项号*/
    return;
}

/**
 * 函数名称: LocSubMenu
 * 函数功能: 计算弹出子菜单区域左上角和右下角的位置.
 * 输入参数: num 选中的主菜单项号
 * 输出参数: rc 存放区域位置信息的地址
 * 返 回 值: 无
 *
 * 调用说明:
 */
void LocSubMenu(int num, SMALL_RECT *rc)
{
    int i, len, loc = 0;

    rc->Top = 1; /*区域的上边定在第2行，行号为1*/
    rc->Left = 1;
    for (i=1; i<num; i++)
    {   /*计算区域左边界位置, 同时计算第一个子菜单项在子菜单字符串数组中的位置*/
        rc->Left += strlen(ga_main_menu[i-1]) + 4;
        loc += ga_sub_menu_count[i-1];
    }
    rc->Right = strlen(ga_sub_menu[loc]);/*暂时存放第一个子菜单项字符串长度*/
    for (i=1; i<ga_sub_menu_count[num-1]; i++)
    {   /*查找最长子菜单字符串，将其长度存放在rc->Right*/
        len = strlen(ga_sub_menu[loc+i]);
        if (rc->Right < len)
        {
            rc->Right = len;
        }
    }
    rc->Right += rc->Left + 3;  /*计算区域的右边界*/
    rc->Bottom = rc->Top + ga_sub_menu_count[num-1] + 1;/*计算区域下边的行号*/
    if (rc->Right >= SCR_COL)  /*右边界越界的处理*/
    {
        len = rc->Right - SCR_COL + 1;
        rc->Left -= len;
        rc->Right = SCR_COL - 1;
    }
    return;
}

/**
 * 函数名称: DealInput
 * 函数功能: 在弹出窗口区域设置热区, 等待并相应用户输入.
 * 输入参数: pHotArea
 *           piHot 焦点热区编号的存放地址, 即指向焦点热区编号的指针
 * 输出参数: piHot 用鼠标单击、按回车或空格时返回当前热区编号
 * 返 回 值:
 *
 * 调用说明:
 */

 //加一个参数进来以确定父文本框的大小
int DealInput(HOT_AREA *pHotArea, int *piHot,SMALL_RECT *box)
{
    INPUT_RECORD inRec;
    DWORD res;
    COORD pos = {0, 0};
    int i,j,num, arrow, iRet = 0;
    int cNo, cTag, cSort;/*cNo:层号, cTag:热区编号, cSort: 热区类型*/
    char vkc, asc;       /*vkc:虚拟键代码, asc:字符的ASCII码值*/

    SetHotPoint(pHotArea, *piHot);
    FlushConsoleInputBuffer(gh_std_in);
    for(i=0,j=0;i<pHotArea->num;i++)
    {
        if(pHotArea->pSort[i]==1)
        {
            DWORD text_box_length;
            pos.X = pHotArea->pArea[i].Left;
            pos.Y = pHotArea->pArea[i].Top;
            SetConsoleCursorPosition(gh_std_out,pos);
            wprintf(L"%s",Input[j++]);
            text_box_length = pHotArea->pArea[*piHot-1].Right - pHotArea->pArea[*piHot-1].Left + 1;
            FillConsoleOutputAttribute(gh_std_out,FOR_LW,text_box_length,pos,&ul);
        }
    }
    for(i=0;i<pHotArea->num;i++)
    {
        if(pHotArea->pSort[i]==1)
        {
            pos.X = pHotArea->pArea[i].Left;
            pos.Y = pHotArea->pArea[i].Top;
            SetConsoleCursorPosition(gh_std_out,pos);
            break;
        }
    }

    while (TRUE)
    {    /*循环*/
        ReadConsoleInputW(gh_std_in, &inRec, 1, &res);
        if ((inRec.EventType == MOUSE_EVENT) &&
            (inRec.Event.MouseEvent.dwButtonState
             == FROM_LEFT_1ST_BUTTON_PRESSED))
        {
            pos = inRec.Event.MouseEvent.dwMousePosition;
            cNo = gp_scr_att[pos.Y * SCR_COL + pos.X] & 3;
            cTag = (gp_scr_att[pos.Y * SCR_COL + pos.X] >> 2) & 15;
            cSort = (gp_scr_att[pos.Y * SCR_COL + pos.X] >> 6) & 3;

//            if( pos.X>box->Right || pos.X<box->Left ||
//                pos.Y>box->Top   || pos.Y<box->Bottom){
//                    if(flag!=1){
//                        flag=1;
//                        continue;
//                    }
//                    *piHot = 0;
//                    return -1;
//                }
//            flag = 0;
            if ((cNo == gp_top_layer->LayerNo) && cTag > 0)
            {
                *piHot = cTag;
                SetHotPoint(pHotArea, *piHot);
                if (cSort == 0)
                {
                    /*返回了选中的层数*/
                    iRet=13;
                    break;
                }
            }
        }
        else if (inRec.EventType == KEY_EVENT && inRec.Event.KeyEvent.bKeyDown)
        {
            vkc = inRec.Event.KeyEvent.wVirtualKeyCode;
            asc = inRec.Event.KeyEvent.uChar.AsciiChar;;
            if (asc == 0)
            {
                arrow = 0;
                switch (vkc)
                {  /*方向键(左、上、右、下)的处理*/
                    case 37: arrow = 1; break;
                    case 38: arrow = 2; break;
                    case 39: arrow = 3; break;
                    case 40: arrow = 4; break;
                }
                if (arrow > 0)
                {
                    num = *piHot;
                    while (TRUE)
                    {
                        if (arrow < 3)
                        {
                            num--;
                        }
                        else
                        {
                            num++;
                        }
                        if ((num < 1) || (num > pHotArea->num) ||
                            ((arrow % 2) && (pHotArea->pArea[num-1].Top
                            == pHotArea->pArea[*piHot-1].Top)) || ((!(arrow % 2))
                            && (pHotArea->pArea[num-1].Top
                            != pHotArea->pArea[*piHot-1].Top)))
                        {
                            break;
                        }
                    }
                    if (num > 0 && num <= pHotArea->num)
                    {
                        *piHot = num;
                        SetHotPoint(pHotArea, *piHot);
                    }
                }
            }
            else if (vkc == 27)
            {  /*ESC键*/
                iRet = 27;
                break;
            }
            else if (vkc == 13)
            {  /*回车键表示按下当前按钮*/
                if((num=*piHot)!=pHotArea->num && pHotArea->pSort[num-1]==1){
                    (*piHot)++;
                    SetHotPoint(pHotArea, *piHot);
                    continue;
                }
                iRet = 13;
                break;
            }
            else if (vkc==8)
            {  /*按下backspace*/
                if(pHotArea->pSort[*piHot-1]!=1)
                    continue;
                int c_pos =  input_pointer[*piHot-1];
                if(c_pos!=0)    input_pointer[*piHot-1]--;
                Input[*piHot-1][c_pos-1] = '\0';
                DWORD write_num,text_box_length;

                pos.X = pHotArea->pArea[*piHot-1].Left;
                pos.Y = pHotArea->pArea[*piHot-1].Top;
                text_box_length = pHotArea->pArea[*piHot-1].Right - pHotArea->pArea[*piHot-1].Left + 1;
                set_background(&(pHotArea->pArea[*piHot-1]),FOR_LW);
                SetConsoleCursorPosition(gh_std_out, pos);
                wprintf(L"%s",Input[*piHot-1]);
                FillConsoleOutputAttribute(gh_std_out,FOR_LW,text_box_length,pos,&write_num);
            }
            else{
                if(pHotArea->pSort[*piHot-1]!=1)
                    continue;
                DWORD write_num,text_box_length;
                text_box_length = pHotArea->pArea[*piHot-1].Right - pHotArea->pArea[*piHot-1].Left + 1;

                WCHAR c_asc = inRec.Event.KeyEvent.uChar.UnicodeChar;
                int c_pos =  input_pointer[*piHot-1];
                if(c_pos>=text_box_length)      continue;
                else           input_pointer[*piHot-1]++;
                Input[*piHot-1][c_pos] = c_asc;
                Input[*piHot-1][c_pos+1] = '\0';

                pos.X = pHotArea->pArea[*piHot-1].Left;
                pos.Y = pHotArea->pArea[*piHot-1].Top;
                SetConsoleCursorPosition(gh_std_out, pos);
                //putwchar(c_asc);
//                char t[20];
//                int nlen = WideCharToMultiByte(CP_ACP,0,Input[*piHot-1],-1,NULL,0,NULL,NULL);
//                WideCharToMultiByte(CP_ACP,0,Input[*piHot-1],-1,t,nlen,NULL,NULL);
                wprintf(L"%s",Input[*piHot-1]);
                FillConsoleOutputAttribute(gh_std_out,FOR_LW,text_box_length,pos,&write_num);
            }

        }
    }

    return iRet;
}

void clean_input_buf()
{
    int i;
    for(i=0;i<6;i++){
        Input[i][0] =L'\0';
        input_pointer[i]=0;
    }
}


void SetHotPoint(HOT_AREA *pHotArea, int iHot)
{
    CONSOLE_CURSOR_INFO lpCur;
    COORD pos = {0, 0};
    WORD att1, att2;
    int i, width;

    att1 = FOREGROUND_BLUE | FOREGROUND_INTENSITY| BACKGROUND_BLUE
            | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY;
    att2 = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY;  /*白底黑字*/
    for (i=0; i<pHotArea->num; i++)
    {  /*将按钮类热区置为白底黑字*/
        pos.X = pHotArea->pArea[i].Left;
        pos.Y = pHotArea->pArea[i].Top;
        width = pHotArea->pArea[i].Right - pHotArea->pArea[i].Left + 1;
        if (pHotArea->pSort[i] == 0)
        {  /*热区是按钮类*/
            FillConsoleOutputAttribute(gh_std_out, att2, width, pos, &ul);
        }
    }

    pos.X = pHotArea->pArea[iHot-1].Left;
    pos.Y = pHotArea->pArea[iHot-1].Top;
    width = pHotArea->pArea[iHot-1].Right - pHotArea->pArea[iHot-1].Left + 1;
    if (pHotArea->pSort[iHot-1] == 0)
    {  /*被激活热区是按钮类*/
        FillConsoleOutputAttribute(gh_std_out, att1, width, pos, &ul);//?
    }
    else if (pHotArea->pSort[iHot-1] == 1)
    {  /*被激活热区是文本框类*/
        SetConsoleCursorPosition(gh_std_out, pos);
        GetConsoleCursorInfo(gh_std_out, &lpCur);
        lpCur.bVisible = TRUE;
        SetConsoleCursorInfo(gh_std_out, &lpCur);
    }
}

/**
 * 函数名称: ExeFunction
 * 函数功能: 执行由主菜单号和子菜单号确定的功能函数.
 * 输入参数: m 主菜单项号
 *           s 子菜单项号
 * 输出参数: 无
 * 返 回 值: BOOL类型, TRUE 或 FALSE
 *
 * 调用说明: 仅在执行函数ExitSys时, 才可能返回FALSE, 其他情况下总是返回TRUE
 */
BOOL ExeFunction(int m, int s)
{
    BOOL bRet = TRUE;
    /*函数指针数组，用来存放所有功能函数的入口地址*/
    BOOL (*pFunction[ga_sub_menu_count[0]+ga_sub_menu_count[1]+ga_sub_menu_count[2]+ga_sub_menu_count[3]+ga_sub_menu_count[4]])(void);
    int i, loc;

    /*将功能函数入口地址存入与功能函数所在主菜单号和子菜单号对应下标的数组元素*/
    pFunction[0] = LoadData;
    pFunction[1] = SaveData;
    pFunction[2] = BackupData;
    pFunction[3] = RestoreData;
    pFunction[4] = ExitSys;
    pFunction[5] = ProvinceDataManage;
    pFunction[6] = AccidentDataManage;
    pFunction[7] = ReportDataManage;
    pFunction[8] = ProvinceDataSearch;
    pFunction[9] = AccidentDataSearch;
    pFunction[10] = ReportDataSearch;
    pFunction[11] = StatisticsView;
//    pFunction[12] = AdvancedStatistic;
//    pFunction[12] = NULL;
//    pFunction[13] = QueryDormInfo;
//    pFunction[14] = QueryStuInfo;
//    pFunction[15] = QueryChargeInfo;
//    pFunction[16] = StatUsedRate;
//    pFunction[17] = StatStuType;
//    pFunction[18] = StatCharge;
//    pFunction[19] = StatUncharge;
//    pFunction[20] = HelpTopic;
//    pFunction[21] = NULL;
//    pFunction[22] = AboutDorm;

    for (i=1,loc=0; i<m; i++)  /*根据主菜单号和子菜单号计算对应下标*/
    {
        loc += ga_sub_real_menu_count[i-1];
    }
    loc += s - 1;

    if (pFunction[loc] != NULL)
    {
        bRet = (*pFunction[loc])();  /*用函数指针调用所指向的功能函数*/
    }

    return bRet;
}

BOOL LoadData()
{
    BOOL bRet = TRUE;
    char *plabel_name[] = {"文件路径：",
                           "确认",
                          };
    int type[2]={1,0};
    int length = 30;
    if(ShowModule(plabel_name,2,type,length)==2)
    {
        int nlen;
        char filepath[30];
        nlen = WideCharToMultiByte(CP_ACP,0,Input[0],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[0],-1,filepath,nlen,NULL,NULL);
        if(readfile(filepath))
            Show_message("成功！");
        else
            Show_message("文件路径不存在");
    }
    return bRet;
}

BOOL SaveData(void)
{
    BOOL flag,bRet = TRUE;
    flag = savefile("data");

    if(flag){
        char *plabel_name[] = {"成功！",
                               "确认",
                              };
        int type[2]={-1,0};
        int length = 15;
        ShowModule(plabel_name,2,type,length);
    }
    else
    {
        char *plabel_name[] = {"失败！",
                               "确认",
                              };
        int type[2]={-1,0};
        int length = 15;
        ShowModule(plabel_name,2,type,length);
    }

    return bRet;
}

BOOL BackupData(void)
{
    BOOL bRet = TRUE;
     char *plabel_name[] = {"保存备份文件路径：",
                           "确认",
                          };
    int type[2]={1,0};
    int length = 20;
    if(ShowModule(plabel_name,2,type,length)==2)
    {
        int nlen;
        char filepath[30];
        nlen = WideCharToMultiByte(CP_ACP,0,Input[0],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[0],-1,filepath,nlen,NULL,NULL);
        if(savefile(filepath))
            Show_message("成功！");
        else
            Show_message("文件路径不存在");
    }

    return bRet;
}

BOOL RestoreData(void)
{
    BOOL bRet = TRUE;
    char *plabel_name[] = {"主菜单项：文件",
                           "子菜单项：数据恢复",
                           "确认"
                          };

//    ShowModule(plabel_name, 3);

    return bRet;
}

BOOL ExitSys(void)
{
    LABEL_BUNDLE labels;
    HOT_AREA areas;
    BOOL bRet = TRUE;
    SMALL_RECT rcPop;
    COORD pos;
    WORD att;
    char *pCh[] = {"确认退出系统吗？", "确定    取消"};
    int iHot = 1;

    pos.X = strlen(pCh[0]) + 6;
    pos.Y = 7;
    rcPop.Left = (SCR_COL - pos.X) / 2;
    rcPop.Right = rcPop.Left + pos.X - 1;
    rcPop.Top = (SCR_ROW - pos.Y) / 2;
    rcPop.Bottom = rcPop.Top + pos.Y - 1;

    att = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;  /*白底黑字*/
    labels.num = 2;
    labels.ppLabel = pCh;
    COORD aLoc[] = {{rcPop.Left+3, rcPop.Top+2},
                    {rcPop.Left+5, rcPop.Top+5}};
    labels.pLoc = aLoc;

    areas.num = 2;
    SMALL_RECT aArea[] = {{rcPop.Left + 5, rcPop.Top + 5,
                           rcPop.Left + 8, rcPop.Top + 5},
                          {rcPop.Left + 13, rcPop.Top + 5,
                           rcPop.Left + 16, rcPop.Top + 5}};
    char aSort[] = {0, 0};
    char aTag[] = {1, 2};
    areas.pArea = aArea;
    areas.pSort = aSort;
    areas.pTag = aTag;
    PopUp(&rcPop, att, &labels, &areas);

    pos.X = rcPop.Left + 1;
    pos.Y = rcPop.Top + 4;
    FillConsoleOutputCharacter(gh_std_out, '-', rcPop.Right-rcPop.Left-1, pos, &ul);

    if (DealInput(&areas, &iHot,&rcPop) == 13 && iHot == 1)
    {
        bRet = FALSE;
    }
    else
    {
        bRet = TRUE;
    }
    PopOff();

    return bRet;
}

BOOL ProvinceDataManage(void)
{
    BOOL bRet = TRUE;
    int selected = 0;
    char *plabel_name[] = {"新增省份信息",
                           "修改省份信息",
                           "删除省份信息",
                           "取消",
                          };
    int type[4] = {0,0,0,0};
    int input_length = 0;
    selected = ShowModule(plabel_name, 4 ,type,input_length);
    if(selected==1)
    {
        char *plabel_name_sub[] = {"省份名：",
                                    "监管负责人：",
                                    "联系电话：",
                                    "确认",
                          };
        int type[4] = {1,1,1,0};
        int input_length = 15;
        if(ShowModule(plabel_name_sub, 4 ,type,input_length)==4)
        {
            struct province_info pro;
            pro_init(&pro);
            WtoProvince(&pro);
            if(pro_has_empty(&pro))
            {
                Show_message("省份信息不完整！");
            }
            else{
                update_province(&pro);
                Show_message("成功！");
            }
        }
    }
    else if(selected==2)
    {
        char *plabel_name_sub[] = { "省份名：",
                                    "确认",
                          };
        int type[2] = {1,0};
        int input_length = 15;
        if(ShowModule(plabel_name_sub, 2 ,type,input_length)==2){
            struct province_info pro,*pnt;
            pro_init(&pro);
            WtoProvince(&pro);
            if(!strcmp(pro.province_name,""))
            {
                Show_message("省份名不为空");
            }
            else if((pnt=locate_province(pro.province_name))==NULL)
            {
                Show_message("该省份不存在");
            }
            else
            {
                clean_input_buf();
                ProvincetoW(pnt);
                char *plabel_name_sub[] = { "省份名：",
                                    "负责人",
                                    "联系电话",
                                    "确认",
                          };
                int type[4] = {1,1,1,0};
                int input_length = 20;
                if(ShowModule(plabel_name_sub, 4 ,type,input_length)==4){
                    pro_init(&pro);
                    WtoProvince(&pro);
                    if(strcmp(pro.province_name,pnt->province_name))
                    {
                        Show_message("不能更改省份名称！");
                    }
                    else if(pro_has_empty(&pro))
                    {
                        Show_message("省份信息不完整！");
                    }
                    else
                    {
                        update_province(&pro);
                        Show_message("修改成功！");
                    }
                }
            }
        }
    }
    else if(selected==3)
    {
        char *plabel_name_sub[] = {"省份名：",
                                    "确认",
                          };
        int type[2] = {1,0};
        int input_length = 15;
        if(ShowModule(plabel_name_sub, 2 ,type,input_length)==2)
        {
            struct province_info pro;
            pro_init(&pro);
            WtoProvince(&pro);
            if(!strcmp(pro.province_name,""))
            {
                Show_message("省份名不能为空");
            }
            else if(delete_province(pro.province_name)==false)
                Show_message("该省份不存在");//show success
            else
                Show_message("成功！");
        }
    }
    return bRet;
}

BOOL AccidentDataManage(void)
{
    BOOL bRet = TRUE;
    int selected = 0;
    char *plabel_name[] = {"新增事故信息",
                           "修改事故信息",
                           "删除事故信息",
                           "确认"
                          };
    int type[4] = {0,0,0,0};
    int input_length = 0;
    selected  = ShowModule(plabel_name, 4,type,input_length);

    if(selected == 1)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "发生时间",
                               "事故类型",
                               "事故等级",
                               "所属省份",
                               "事故发生单位",
                               "死亡人数",
                               "重伤人数",
                               "直接经济损失",
                               "确认",
                          };
        int type[10] = {1,1,1,1,1,1,1,1,1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,10,type,input_length)==10)
        {
            struct accident_info acc;
            acc_init(&acc);
            WtoAccident(&acc);
            if(acc_has_empty(&acc))
            {
                Show_message("事故信息不完整！");
            }
            else if(update_accident(&acc)==false)
            {
                Show_message("事故所属省份不存在！");
            }
            else
                Show_message("成功！");
        }
    }
    else if(selected == 2)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "确认",
                          };
        int type[2] = {1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,2,type,input_length)==2)
        {
            struct accident_info acc,*pnt;
            acc_init(&acc);
            WtoAccident(&acc);
            if(!strcmp(acc.accident_id,""))
            {
                Show_message("事故编号不能为空！");
            }
            else if((pnt=locate_accident(acc.accident_id))==NULL)
            {
                Show_message("事故信息不存在！");
            }
            else
            {
                clean_input_buf();
                AccidenttoW(pnt);
                char *plabel_name_sub[] = {"事故编号",
                               "发生时间",
                               "事故类型",
                               "事故等级",
                               "所属省份",
                               "事故发生单位",
                               "死亡人数",
                               "重伤人数",
                               "直接经济损失",
                               "确认",
                          };
                int type[10] = {1,1,1,1,1,1,1,1,1,0};
                int input_length = 20;
                if(ShowModule(plabel_name_sub,10,type,input_length)==10)
                {
                    acc_init(&acc);
                    WtoAccident(&acc);
                    if(acc_has_empty(&acc))
                    {
                        Show_message("事故信息不完整！");
                    }
                    else if(strcmp(acc.accident_id,pnt->accident_id))
                    {
                        Show_message("不能更改事件id！");
                    }
                    else{
                        update_accident(&acc);
                        Show_message("成功！");
                    }
                }
            }
        }
    }
    else if(selected == 3)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "确认",
                          };
        int type[2] = {1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,2,type,input_length)==2)
        {
            struct accident_info acc;
            acc_init(&acc);
            WtoAccident(&acc);
            if(!strcmp(acc.accident_id,""))
            {
                Show_message("事故编号不能为空！");
            }
            else if(delete_accident(acc.accident_id)==false)
            {
                Show_message("事故信息不存在！");
            }
            else
                Show_message("成功！");
        }
    }

    return bRet;
}

BOOL ReportDataManage(void)
{
    BOOL bRet = TRUE;
    int selected = 0;
    char *plabel_name[] = {"新增事故报告信息",
                           "修改事故报告信息",
                           "删除事故报告信息",
                           "确认",
                          };
    int type[4] = {0,0,0,0};
    int input_length = 0;
    selected = ShowModule(plabel_name, 4,type,input_length);

    if(selected == 1)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "报道日期",
                               "媒体类别",
                               "媒体名称",
                               "内容索引",
                               "确认",
                          };
        int type[6] = {1,1,1,1,1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,6,type,input_length)==6)
        {
            struct report_info rpo;
            report_init(&rpo);
            WtoReport(&rpo);
            if(rpo_has_empty(&rpo))
            {
                Show_message("事故报道信息不完整");
            }
            else if(update_report(&rpo)==false)
            {
                Show_message("事故报道所属事故信息不存在");
            }
            else
                Show_message("成功！");
        }
    }
    else if(selected == 2)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "媒体名称",
                               "确认",
                          };
        int type[3] = {1,1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,2,type,input_length)==2)
        {
            struct report_info rpo,*pnt;
            report_init(&rpo);
            WtoReport_E(&rpo);
            if(!strcmp(rpo.accident_id,"")&&!strcmp(rpo.media_name,""))
            {
                Show_message("事故编号和媒体名称信息不能为空！");
            }
            else if((pnt=locate_report(rpo.accident_id,rpo.media_name))==NULL)
            {
                Show_message("事故报道信息不存在！");
            }
            else
            {
                char *plabel_name_sub[] = {"事故编号",
                               "报道日期",
                               "媒体类别",
                               "媒体名称",
                               "内容索引",
                               "确认",
                          };
                int type[6] = {1,1,1,1,1,0};
                int input_length = 20;
                if(ShowModule(plabel_name_sub,6,type,input_length)==6)
                {
                    struct report_info rpo;
                    report_init(&rpo);
                    WtoReport(&rpo);
                    if(rpo_has_empty(&rpo))
                    {
                        Show_message("事故报道信息不完整");
                    }
                    else if(strcmp(rpo.accident_id,pnt->accident_id)||strcmp(rpo.media_name,pnt->media_name))
                    {
                        Show_message("事故报道所属事故信息不存在");
                    }
                    else{
                        update_report(&rpo);
                        Show_message("成功！");
                    }
                }
            }
        }
    }
    else if(selected == 3)
    {
        char *plabel_name_sub[] = {"事故编号",
                               "媒体名称",
                               "确认",
                          };
        int type[3] = {1,1,0};
        int input_length = 20;
        if(ShowModule(plabel_name_sub,3,type,input_length)==3)
        {
            struct report_info rpo;
            report_init(&rpo);
            WtoReport(&rpo);
            if(!strcmp(rpo.accident_id,"")&&!strcmp(rpo.media_name,""))
            {
                Show_message("事故编号和媒体名称信息不能为空！");
            }
            else if(delete_report(rpo.accident_id,rpo.media_name)==false)
            {
                Show_message("事故报道信息不存在！");
            }
            else
                Show_message("成功！");
        }
    }
    return bRet;
}

BOOL ProvinceDataSearch(void)
{
    BOOL bRet = TRUE;
    char *plabel_name[] = {"省份名字",
                           "省份负责人",
                           "联系电话",
                           "确认",
                          };
    int type[4] = {1,1,1,0};
    int input_length = 20;

    if(ShowModule(plabel_name, 4 ,type,input_length)==4)
    {
        struct province_info pro;
        pro_init(&pro);
        WtoProvince(&pro);
        int total;

        if((total=search_province(&pro))==0){
            Show_message("未找到任何结果");
        }
        else
        {
            Show_message("成功！");
            Show_province(total,1);
            ShowState("省份信息查询结果");
        }
    }
    //
    return bRet;
}

BOOL AccidentDataSearch(void)
{
    BOOL bRet = TRUE;
    char *plabel_name[] = {"事件编号",
                           "事件发生时间",
                           "事件类型",
                           "事件等级",
                           "事件所属省份",
                           "事件责任公司",
                           "确认",
                          };
    int type[7] = {1,1,1,1,1,1,0};
    int input_length = 20;

    if(ShowModule(plabel_name,7,type,input_length)==7)
    {
        struct accident_info acc;
        acc_init(&acc);
        WtoAccident_S(&acc);
        int total;
        if((total=search_accident(&acc))==0)
            Show_message("未找到符合条件结果");
        else{
            Show_message("成功！");
            Show_accident(total,1);
            ShowState("事故信息查询结果");
        }
    }
    return bRet;
}

BOOL ReportDataSearch(void)
{
    BOOL bRet = TRUE;
    char *plabel_name[] = {"报告事件编号",
                           "报告时间",
                           "报告媒体类别",
                           "报告媒体名称（部分名称）",
                           "确认",
                          };
    int type[5] = {1,1,1,1,0};
    int input_length = 20;
    if(ShowModule(plabel_name,5,type,input_length)==5)
    {
        struct report_info rpo;
        report_init(&rpo);
        WtoReport_S(&rpo);
        int total;
        if((total=search_report(&rpo))==0)
            Show_message("未找到符合条件结果");
        else{
            Show_message("成功！");
            Show_report(total,1);
            ShowState("report data search result");
        }
    }


    return bRet;
}

BOOL StatisticsView(void)
{
    BOOL bRet = TRUE;
    int selected = 0;
    char *plabel_name[] = {"各省事故累计情况",
                           "年度事故等级统计",
                           "各类事故累计情况",
                           "媒体焦点事故统计",
                           "取消",
                          };
    int type[5] = {0,0,0,0,0};
    int input_length = 0;
    selected = ShowModule(plabel_name, 5 ,type,input_length);

    if(selected==1)
    {
        if(P_accident_count_by_pro!=NULL)
            free(P_accident_count_by_pro);
        P_accident_count_by_pro = accident_count_by_pro();
        Show_accident_count_by_pro(PROVINCE_NUM,1);
        ShowState("accident statistics for each province");
    }
    else if (selected == 2)
    {
        int *p,nlen;
        char a[10];
        char *sub_plabel_name[] = {"年份：","确认",};
        int sub_type[2] = {1,0};
        int sub_input_length = 6;
        if(ShowModule(sub_plabel_name, 2 ,sub_type,sub_input_length)==2)
        {
            nlen = WideCharToMultiByte(CP_ACP,0,Input[0],-1,NULL,0,NULL,NULL);
            WideCharToMultiByte(CP_ACP,0,Input[0],-1,a,nlen,NULL,NULL);
            p = accident_yearly_count(a);

            char *grade[]={"特大","重大","较大","一般"};
            ClearScreen();
            COORD cursor_pos,pos = {4,3};
            int i;
            DWORD len;
            SetConsoleCursorPosition(gh_std_out,pos);
            printf("级别    发生次数");
            FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,pos,&len);
            for(i=0;i<4;i++)
            {
                cursor_pos.Y = pos.Y+1+i;
                cursor_pos.X = pos.X;
                SetConsoleCursorPosition(gh_std_out,cursor_pos);
                printf("%-8s%d",grade[i],p[i]);
                FillConsoleOutputAttribute(gh_std_out,FRO_BLACK_BAC_W,72,cursor_pos,&len);
            }
        }
        char message[40] = "各级别事故发生情况 年份：";
        strcat(message,a);
        ShowState(message);
        free(p);
    }
    else if (selected == 3)
    {
        if(P_accident_count_by_type!=NULL)
            free(P_accident_count_by_type);
        P_accident_count_by_type = accident_count_by_type();
        Show_accident_count_by_type(ACC_TYPE_NUM,1);
        ShowState("各种类事故发生信息");
    }
    else if (selected == 4)
    {
        if(P_accident_sort_by_report!=NULL)
            free(P_accident_sort_by_report);
        P_accident_sort_by_report = accident_sort_by_report();
        Show_accident_sort_by_report(10,1);
        ShowState("10大焦点事故");
    }
    return bRet;
}
int ShowModule(char **pString, int n,int *type,int input_length)
{
    LABEL_BUNDLE labels;
    HOT_AREA areas;
    int num=0;
    SMALL_RECT rcPop;
    COORD pos;
    WORD att;
    int iHot = 1;
    int i,j, maxlen, str_len,input_lable_max;

    for (i=0,maxlen=0,input_lable_max=0 ; i<n; i++) {
        str_len = strlen(pString[i]);
        if (maxlen < str_len) {
            maxlen = str_len;
        }
        if(type[i]==1){
            if(maxlen< str_len+input_length)
                maxlen = str_len+input_length;
            input_lable_max = input_lable_max>str_len?input_lable_max:str_len;
        }//
    }

    pos.X = maxlen + 6;
    pos.Y = 2*n + 4;
    rcPop.Left = (SCR_COL - pos.X) / 2;
    rcPop.Right = rcPop.Left + pos.X - 1;
    rcPop.Top = (SCR_ROW - pos.Y) / 2;
    rcPop.Bottom = rcPop.Top + pos.Y - 1;

    att = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED |BACKGROUND_INTENSITY;  /*白底黑字*/
    labels.num = n;
    labels.ppLabel = pString;
    COORD aLoc[n];

    for (i=0; i<n; i++) {
        aLoc[i].X = rcPop.Left + 3;
        aLoc[i].Y = rcPop.Top + 2 + 2*i;
    }

    str_len = strlen(pString[n-1]);
    aLoc[n-1].X = rcPop.Left + 3 + (maxlen-str_len)/2;
    aLoc[n-1].Y = aLoc[n-1].Y + 2;

    labels.pLoc = aLoc;
                                                        //*
    areas.num = n;
    SMALL_RECT aArea[n];
    char aSort[n];
    char aTag[n];
    for(j=0,i=0;i<n;i++)
    {
        if(type[i]==-1){                //只显示文本的情况
            areas.num--;
            continue;
        }
        if(type[i]==0){
            aArea[j].Left = aLoc[i].X;          aArea[j].Top =  aLoc[i].Y;
            aArea[j].Right= aLoc[i].X+maxlen;   aArea[j].Bottom=aLoc[i].Y;
        }
        else if(type[i]==1){
            aArea[j].Left = aLoc[i].X+input_lable_max+1;        aArea[j].Top =  aLoc[i].Y;
            aArea[j].Right= aArea[i].Left+input_length-1;         aArea[j].Bottom=aLoc[i].Y;
        }
        aSort[j] = type[i];
        aTag[j]  = i+1;
        j++;
    }
    aArea[areas.num-1].Left = aLoc[n-1].X;     aArea[areas.num-1].Top =  aLoc[n-1].Y;
    aArea[areas.num-1].Right= aLoc[n-1].X+3;   aArea[areas.num-1].Bottom=aLoc[n-1].Y;

    areas.pArea = aArea;
    areas.pSort = aSort;
    areas.pTag = aTag;
    PopUp(&rcPop, att, &labels, &areas);
                                                        //*
    pos.X = rcPop.Left + 1;
    pos.Y = rcPop.Top + 2*n;
    FillConsoleOutputCharacter(gh_std_out, '-', rcPop.Right-rcPop.Left-1, pos, &ul);

    for(i=0;i<n;i++)
    {
        if(type[i]!=1)      continue;
        COORD activate_pos;
        activate_pos.X = aLoc[i].X + input_lable_max+1;
        activate_pos.Y = aLoc[i].Y;
        SMALL_RECT area = {activate_pos.X,activate_pos.Y,
                activate_pos.X+input_length-1,activate_pos.Y};

        set_background(&area,att);
    }

    num = DealInput(&areas, &iHot,&rcPop);
    PopOff();

    if(num==13)
        return iHot;
    else
        return -1;

}

void set_background(SMALL_RECT *area,WORD att)
{
    CHAR_INFO *lpbuffer;
    COORD size;
    COORD pos  = {0,0};
    int j;

    size.X = area->Right - area->Left +1;
    size.Y = area->Bottom- area->Top  +1;
    lpbuffer = (CHAR_INFO *)malloc(sizeof(CHAR_INFO)*size.X*size.Y);
    for(j=0;j<size.X*size.Y;j++)
    {
        lpbuffer[j].Char.AsciiChar = ' ';
        lpbuffer[j].Attributes     = att;
    }
    WriteConsoleOutput(gh_std_out,lpbuffer,size,pos,area);
    free(lpbuffer);
}

void WtoProvince(struct province_info *pro)
{
    char temp[3][30];
    int nlen,i;
    for(i=0;i<3;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(pro->province_name,temp[0]);
    strcpy(pro->responsible_pname,temp[1]);
    strcpy(pro->contact_number,temp[2]);
}
void WtoAccident(struct accident_info *acc)
{
    char temp[9][30];
    int nlen,i;
    for(i=0;i<9;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(acc->accident_id,temp[0]);
    strcpy(acc->occur_time,temp[1]);
    strcpy(acc->accident_type,temp[2]);
    acc->accident_grade = temp[3][0];
    strcpy(acc->province_location,temp[4]);
    strcpy(acc->responsible_company,temp[5]);
    acc->death_amount = atoi(temp[6]);
    acc->injured_amount = atoi(temp[7]);
    acc->financial_wastage = atof(temp[8]);
}

void WtoReport(struct report_info *rpo)
{
    char temp[5][30];
    int nlen,i;
    for(i=0;i<5;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(rpo->accident_id,temp[0]);
    strcpy(rpo->report_date,temp[1]);
    rpo->media_type = temp[2][0];
    strcpy(rpo->media_name,temp[3]);
    strcpy(rpo->detail_url,temp[4]);
}


void WtoAccident_S(struct accident_info *acc)
{
    char temp[6][30];
    int nlen,i;
    for(i=0;i<6;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(acc->accident_id,temp[0]);
    strcpy(acc->occur_time,temp[1]);
    strcpy(acc->accident_type,temp[2]);
    acc->accident_grade = temp[3][0];
    strcpy(acc->province_location,temp[4]);
    strcpy(acc->responsible_company,temp[5]);
}
void WtoReport_S(struct report_info *rpo)
{
    char temp[4][30];
    int nlen,i;
    for(i=0;i<4;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(rpo->accident_id,temp[0]);
    strcpy(rpo->report_date,temp[1]);
    rpo->media_type = temp[2][0];
    strcpy(rpo->media_name,temp[3]);
}

void WtoReport_E(struct report_info *rpo)
{
    char temp[2][30];
    int nlen,i;
    for(i=0;i<2;i++)
    {
        nlen = WideCharToMultiByte(CP_ACP,0,Input[i],-1,NULL,0,NULL,NULL);
        WideCharToMultiByte(CP_ACP,0,Input[i],-1,temp[i],nlen,NULL,NULL);
    }
    strcpy(rpo->accident_id,temp[0]);
    strcpy(rpo->media_name,temp[1]);
}


void ProvincetoW(struct province_info *pro)
{
    char temp[3][30];
    int len,i;
    strcpy(temp[0],pro->province_name);
    strcpy(temp[1],pro->responsible_pname);
    strcpy(temp[2],pro->contact_number);

    for(i=0;i<3;i++)
    {
        len = MultiByteToWideChar(CP_ACP,0,temp[i],-1,NULL,0);
        MultiByteToWideChar(CP_ACP,0,temp[i],-1,Input[i],len);
        input_pointer[i] = len-1;
    }
}

void AccidenttoW(struct accident_info *acc)
{
    char temp[9][30];
    int len,i;
    strcpy(temp[0],acc->accident_id);
    strcpy(temp[1],acc->occur_time);
    strcpy(temp[2],acc->accident_type);
    temp[3][0] = acc->accident_grade;   temp[3][1]='\0';
    strcpy(temp[4],acc->province_location);
    strcpy(temp[5],acc->responsible_company);
    itoa(acc->death_amount,temp[6],10);
    itoa(acc->injured_amount,temp[7],10);
    sprintf(temp[8],"%f",acc->financial_wastage);
    temp[8][strlen(temp[8])]='\0';

    for(i=0;i<9;i++)
    {
        len = MultiByteToWideChar(CP_ACP,0,temp[i],-1,NULL,0);
        MultiByteToWideChar(CP_ACP,0,temp[i],-1,Input[i],len);
        input_pointer[i] = len-1;
    }
}

void ReporttoW(struct report_info *rpo)
{
    char temp[5][60];
    int len,i;
    strcpy(temp[0],rpo->accident_id);
    strcpy(temp[1],rpo->report_date);
    temp[2][0] = rpo->media_type;   temp[2][1]='\0';
    strcpy(temp[3],rpo->media_name);
    strcpy(temp[4],rpo->detail_url);

    for(i=0;i<9;i++)
    {
        len = MultiByteToWideChar(CP_ACP,0,temp[i],-1,NULL,0);
        MultiByteToWideChar(CP_ACP,0,temp[i],-1,Input[i],len);
        input_pointer[i] = len-1;
    }
}
