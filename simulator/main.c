#include "defines.h"
/***************************************************************************/

int (*ops[])(void) = {HLT,JMP,CJMP,OJMP,CALL,RET,PUSH,POP,
                      LOADB,LOADW,STOREB,STOREW,LOADI,NOP,IN,OUT,
                      ADD,ADDI,SUB,SUBI,MUL,DIV,AND,OR,
                      NOR,NOTB,SAL,SAR,EQU,LT,LTE,NOTC};

int initialize(char * file)
{
    CS = MEM;
    DS = MEM+MAX_MEOMORY/4;
    SS = MEM+MAX_MEOMORY/4*2;
    ES = MEM+MAX_MEOMORY/4*3;
    PC = (unsigned long*)CS;

    FILE * fp;
    fp = fopen(file,"r");

    if(fp == NULL)
    {
     printf("file doesn't exist");
     exit(-1);
    }

    char op[12];
    unsigned long *pnt = (unsigned long*)CS;
    unsigned long total=0;

    fgets(op,12,fp);
    int i;
    for(i=2;i<10;i++)                                                   //translate code from char to unsigned long
    {
        unsigned long num;
        if(isdigit(op[i]))
        num = op[i] - '0';
        else
        num = op[i] - 'a'+10;

        total = total + (num<<(9-i)*4);
    }


    while(total!=0 && fgets(op,12,fp)!=NULL)
    {
        unsigned long ir = 0;
        int i;
        for(i=2;i<10;i++)                                                   //translate code from char to unsigned long
        {
            unsigned long num;

            if(isdigit(op[i]))
			num = op[i] - '0';
			else
			num = op[i] - 'a'+10;

            ir = ir + (num<<(9-i)*4);
        }
//        printf("%lx\n",ir);
        *pnt++ = ir;
        total--;                                                    //load machine code into memory
    }

    pnt = (unsigned long*)DS;
    while(fgets(op,12,fp)!=NULL)
    {
        unsigned long initial_val = 0;
        int i;
        for(i=2;i<10;i++)                                                   //translate code from char to unsigned long
        {
            unsigned long num;

            if(isdigit(op[i]))
			num = op[i] - '0';
			else
			num = op[i] - 'a'+10;

            initial_val = initial_val + (num<<(9-i)*4);
        }
        //printf("%lx\n",initial_val);
        *pnt++ = initial_val;
    }

    return 1;
}


int main(int argc,char **argv)
{
    initialize(argv[1]);
//    int i;
//    for(i=0;i<10;i++)
//    {
 //       printf("%d\n",*(DS+i));
 //   }
 //   freopen("jojo.txt","w",stdout);
    while(1)
    {
        IR = *PC++;
        if(!ops[OP]())
            break;
    }
    return 0;
}
