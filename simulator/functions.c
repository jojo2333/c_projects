#include "defines.h"
int HLT()
{
    //停机
    return 0;
}

int JMP()
{
    //跳转
    PC = (unsigned long*)(ADDRESS + (char *)CS);
    return 1;
}

int CJMP()
{
    //条件跳转
    if(PSW.compare_flg)
        PC = (unsigned long*)(ADDRESS + (char *)CS);
    return 1;
}

int OJMP()
{
    //溢出跳转
    if(PSW.overflow_flg)
        PC = (unsigned long*)(ADDRESS + (char *)CS);
    return 1;
}

int CALL()
{
    //调用
    struct STORE_CONDITION s;
    int i;

    for(i=0;i<8;i++)    s.common_reg[i] = GR[i];
    s.program_counter = PC;
    s.stat_word = PSW;

    *(struct STORE_CONDITION*)ES = s;                                            //***
    ES += sizeof(struct STORE_CONDITION);

    PC = (unsigned long *)((char*)CS + ADDRESS);
    return 1;
}

int RET()
{
    //返回
    ES -= sizeof(struct STORE_CONDITION);
    struct STORE_CONDITION s = *(struct STORE_CONDITION *)ES;
    int i;

    for(i=0;i<8;i++)    GR[i] = s.common_reg[i];
    PC = s.program_counter;
    PSW= s.stat_word;

    return 1;
}

int PUSH()
{
    //压栈
    short *pnt = (short *)SS;
    *pnt = GR[REG0];
    pnt++;
    SS = (unsigned char *)pnt;

    return 1;
}

int POP()
{
    //出栈
    short *pnt = (short *)SS;
    --pnt;
    GR[REG0] = *pnt;
    SS = (unsigned char*)pnt;

    return 1;
}

int LOADB()
{
    //加载某一字节的数据
    char *symbol = ADDRESS+(char *)DS;
    GR[REG0] = symbol[GR[7]];

    return 1;
}

int LOADW()
{
    //加载两个字节的数据
    short *symbol = (short *)(ADDRESS+(char *)DS);
    GR[REG0] = symbol[GR[7]];

    return 1;
}

int STOREB()
{
   //储存一个字节的数据
    char *symbol = (char *)((char *)DS + ADDRESS);
    symbol[GR[7]] = GR[REG0];

    return 1;
}

int STOREW()
{
    //储存某两个字节的数据
    short *symbol = (short *)(ADDRESS+(char *)DS);
    symbol[GR[7]] = GR[REG0];

    return 1;
}

int LOADI()
{
    //储存一个立即数
    GR[REG0] = (short)IMMEDIATE;
    return 1;
}

int NOP()
{
    //消耗一个指令周期
    return 1;
}

int IN()
{
    //键盘输入
    read(0,(void *)(GR+REG0),1);
    return 1;
}

int OUT()
{
    //
    fflush(stdout);
    write(1,(void *)(GR+REG0),1);
    return 1;
}

int ADD()
{
    //printf("ADD\n");
    GR[REG0] = GR[REG1]+GR[REG2];
    if(GR[REG2]>0)
    {
        if(GR[REG0]<GR[REG1])
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else if(GR[REG2]<0)
    {
        if(GR[REG0]>GR[REG1])
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else
            PSW.overflow_flg = 0;

    return 1;
}

int ADDI()
{
    //printf("ADDI\n");
    short i = GR[REG0];
    GR[REG0] = GR[REG0]+(short)IMMEDIATE;
    if((short)IMMEDIATE>0)
    {
        if(GR[REG0]<i)
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else if((short)IMMEDIATE<0)
    {
        if(GR[REG0]>i)
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else
            PSW.overflow_flg = 0;

    return 1;
}

int SUB()
{
    //printf("SUB\n");
    GR[REG0] = GR[REG1] - GR[REG2];
    if(GR[REG2]>0)
    {
        if(GR[REG0]>GR[REG1])
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else if(GR[REG2]<0)
    {
        if(GR[REG0]<GR[REG1])
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else    PSW.overflow_flg = 1;

    return 1;
}

int SUBI()
{
    //printf("SUBI\n");
    short i = GR[REG0];
    GR[REG0] = GR[REG0] - (short)IMMEDIATE;
    if((short)IMMEDIATE>0)
    {
        if(GR[REG0]>i)
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else if((short)IMMEDIATE<0)
    {
        if(GR[REG0]<i)
            PSW.overflow_flg = 1;
        else
            PSW.overflow_flg = 0;
    }
    else    PSW.overflow_flg = 1;

    return 1;
}

int MUL()
{
    //printf("MUL\n");
    int a = GR[REG1],b = GR[REG2];
    GR[REG0] = GR[REG1]*GR[REG2];
    if(a*b == GR[REG0])
        PSW.overflow_flg = 0;
    else
        PSW.overflow_flg = 1;

    return 1;
}

int DIV()
{
   // printf("DIV\n");
    if(GR[REG2]==0)
    {
        printf("div 0 error!");
        exit(-1);
    }
    GR[REG0] = GR[REG1]/GR[REG2];

    return 1;
}

int AND()
{
    //printf("AND\n");
    GR[REG0] = GR[REG1]&GR[REG2];
    return 1;
}

int OR()
{
    //printf("OR\n");
    GR[REG0] = GR[REG1]|GR[REG2];
    return 1;
}

int NOR()
{
    //printf("NOR\n");
    GR[REG0] = GR[REG1]^GR[REG2];
    return 1;
}

int NOTB()
{
    //printf("NOTB\n");
    GR[REG0] = ~GR[REG1];
    return 1;
}

int SAL()
{
    //printf("SAL\n");
    GR[REG0] = GR[REG1]<<GR[REG2];
    return 1;
}

int SAR()
{
    //printf("SAR\n");
    GR[REG0] = GR[REG1]>>GR[REG2];
    return 1;
}


int EQU()
{
    //printf("EQU\n");
    PSW.compare_flg = (GR[REG0]==GR[REG1]);
    return 1;
}

int LT()
{
    //printf("LT\n");
    PSW.compare_flg = (GR[REG0]<GR[REG1]);
    return 1;
}

int LTE()
{
    //printf("LTE\n");
    PSW.compare_flg = (GR[REG0]<=GR[REG1]);
    return 1;
}


int NOTC()
{
    //printf("NOTC\n");
    PSW.compare_flg = ~PSW.compare_flg;
    return 1;
}


