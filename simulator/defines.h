#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <string.h>
#include <ctype.h>

#define  OP        ((IR>>27)&0x1F)
#define  REG0      ((IR>>24)&7)
#define  REG1      ((IR>>20)&0xF)
#define  REG2      ((IR>>16)&0xF)
#define  IMMEDIATE (IR&0xFFFF)
#define  ADDRESS   (IR&0xFFFFFF)

#define  MAX_MEOMORY (1<<20)




/*memory*/
unsigned char  MEM[MAX_MEOMORY];




/*register*/
unsigned char* CS,*DS,*SS,*ES;                                               //pointing to four memory block
unsigned long*  PC;                                                         //program counter len of instructions are 4byte (unsigned long)
unsigned long   IR;
short        GR[8];                                                         //corresponding 0:Z 1:A 2:B 3:C 4:D 5:E 6:F 7:G
struct PROG_STAT_WORD{
    unsigned short overflow_flg:1;
    unsigned short  compare_flg:1;
    unsigned short     reserve:14;
}PSW;
struct STORE_CONDITION{                                                     //use to pack up information and store it in ES
    short common_reg[8];
    struct PROG_STAT_WORD stat_word;
    unsigned long * program_counter;
};


/*ALU*/
int HLT();   int JMP();   int CJMP();  int OJMP();  int CALL(); int RET();  int PUSH(); int POP();
int LOADB(); int LOADW(); int STOREB();int STOREW();int LOADI();int NOP();  int IN();   int OUT();
int ADD();   int ADDI();  int SUB();   int SUBI();  int MUL();  int DIV();  int AND();  int OR();
int NOR();   int NOTB();  int SAL();   int SAR();   int EQU();  int LT();   int LTE();  int NOTC();


