import random
import string
PROVINCE = ["北京市","天津市","上海市","重庆市","河北省","山西省","辽宁省","吉林省","黑龙江省",
    "江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","海南省",
    "四川省","贵州省","云南省","陕西省","甘肃省","青海省","台湾省","内蒙古自治区","广西壮族自治区","西藏自治区",
    "宁夏回族自治区","新疆维吾尔自治区","香港特别行政区","澳门特别行政区"]
RESPONSIBLE_MAN = ["习近平","李克强","张德江","俞正声","刘云山","王岐山","张高丽"]
ACCIDENT_TYPE  = ["触电","火灾","灼烫","淹溺","高处坠落","坍塌","透水","火药爆炸","瓦斯爆炸","锅炉爆炸","其他爆炸","中毒和窒息","其他伤害"]
MEDIA_NAME    = ["xinlang","bilibili","baidu","cctv","wuhanchenbao","wuhanwanbao","changjiangribao","renmingribao","xinhuashe"]
COMPANY       = "abcdefghijklmnopqrstuvwxyz"

f = open("data_sample_small.txt","w")

for pro in PROVINCE:
    f.write("#province\n")
    f.write(pro+'\n')
    f.write(random.choice(RESPONSIBLE_MAN)+'\n')
    f.write("{0:0>11}\n\n".format(str(random.randint(0,99999999999))))
    for i in range(1,20):
        year = "{0:0>2}".format(str(random.randint(1980,2016)))
        month= "{0:0>2}".format(str(random.randint(1,12)))
        day  = "{0:0>2}".format(str(random.randint(1,31)))
        _id  = "{0:0>3}".format(str(random.randint(1,999)))
        hour = "{0:0>2}".format(str(random.randint(0,24)))
        minute="{0:0>2}".format(str(random.randint(0,59)))
        f.write("#accident\n")
        f.write(year+month+day+_id+'\n')
        f.write(year+month+day+'-'+hour+':'+minute+'\n')
        f.write(random.choice(ACCIDENT_TYPE)+'\n')
        f.write(random.choice(['1','2','3','4'])+'\n')
        f.write(pro+'\n')
        f.write(random.choice(COMPANY)+'\n')
        f.write(str(random.randint(0,200))+'\n')
        f.write(str(random.randint(0,1000))+'\n')
        f.write(str(random.uniform(0,10000))+'\n'+'\n')
        for j in range(1,5):
            media_name = random.choice(MEDIA_NAME)
            f.write("#report\n")
            f.write(year+month+day+_id+'\n')
            f.write(year+month+day+'\n')
            f.write(random.choice(['1','2','3','4'])+'\n')
            f.write(media_name+'\n')
            f.write("http://www."+media_name+".com/"+year+month+day+"/"+"".join(random.sample("abcdefghijklmnopqrstuvwxyz1234567890",10))+'\n'+'\n')

f.close()
