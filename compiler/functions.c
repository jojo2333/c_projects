#include "defines.h"
/*
    all functions using the same kind of parameter which will be explained differently
*/

UL HLT(UL V1,UL V2,UL V3)
{
    return 0;
}
UL JMP(UL ADDR,UL V1,UL V2)
{
    return ((1<<27)|ADDR);
}
UL CJMP(UL ADDR,UL V1,UL V2)
{
    return ((2<<27)|ADDR);
}
UL OJMP(UL ADDR,UL V1,UL V2)
{
    return ((3<<27)|ADDR);
}
UL CALL(UL ADDR,UL V1,UL V2)
{
    return ((4<<27)|ADDR);
}
UL RET(UL V1,UL V2,UL V3)
{
    return (5<<27);
}
UL PUSH(UL REG0,UL V1,UL V2)
{
    return ((6<<27)|(REG0<<24));
}
UL POP(UL REG0,UL V1,UL V2)
{
    return ((7<<27)|(REG0<<24));
}
UL LOADB(UL REG0,UL ADDR,UL V1)
{
    return ((8<<27)|(REG0<<24)|ADDR);
}
UL LOADW(UL REG0,UL ADDR,UL V1)
{
    return ((9<<27)|(REG0<<24)|ADDR);
}
UL STOREB(UL REG0,UL ADDR,UL V1)
{
    return ((10<<27)|(REG0<<24)|ADDR);
}
UL STOREW(UL REG0,UL ADDR,UL V1)
{
    return ((11<<27)|(REG0<<24)|ADDR);
}
UL LOADI(UL REG0,UL IMM,UL V1)
{
    return ((12<<27)|(REG0<<24)|IMM);
}
UL NOP(UL V1,UL V2,UL V3)
{
    return (13<<27);
}
UL IN(UL REG0,UL PORT,UL V1)
{
    return ((14<<27)|(REG0<<24)|PORT);
}
UL OUT(UL REG0,UL PORT,UL V1)
{
    return ((15<<27)|(REG0<<24)|PORT);
}
UL ADD(UL REG0,UL REG1,UL REG2)
{
    return ((16<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL ADDI(UL REG0,UL IMM,UL V1)
{
    return ((17<<27)|(REG0<<24)|IMM);
}
UL SUB(UL REG0,UL REG1,UL REG2)
{
    return ((18<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL SUBI(UL REG0,UL IMM,UL V1)
{
    return ((19<<27)|(REG0<<24)|IMM);
}
UL MUL(UL REG0,UL REG1,UL REG2)
{
    return ((20<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL DIV(UL REG0,UL REG1,UL REG2)
{
    return ((21<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL AND(UL REG0,UL REG1,UL REG2)
{
    return ((22<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL OR(UL REG0,UL REG1,UL REG2)
{
    return ((23<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL NOR(UL REG0,UL REG1,UL REG2)
{
    return ((24<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL NOTB(UL REG0,UL REG1,UL V1)
{
    return ((25<<27)|(REG0<<24)|(REG1<<20));
}
UL SAL(UL REG0,UL REG1,UL REG2)
{
    return ((26<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL SAR(UL REG0,UL REG1,UL REG2)
{
    return ((27<<27)|(REG0<<24)|(REG1<<20)|(REG2<<16));
}
UL EQU(UL REG0,UL REG1,UL V1)
{
    return ((28<<27)|(REG0<<24)|(REG1<<20));
}
UL LT(UL REG0,UL REG1,UL V1)
{
    return ((29<<27)|(REG0<<24)|(REG1<<20));
}
UL LTE(UL REG0,UL REG1,UL V1)
{
    return ((30<<27)|(REG0<<24)|(REG1<<20));
}
UL NOTC(UL V1,UL V2,UL V3)
{
    return (31<<27);
}
