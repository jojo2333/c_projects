#ifndef jojo
#define jojo
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
typedef unsigned long UL;
UL HLT(UL V1,UL V2,UL V3);
UL JMP(UL ADDR,UL V1,UL V2);
UL CJMP(UL ADDR,UL V1,UL V2);
UL OJMP(UL ADDR,UL V1,UL V2);
UL CALL(UL ADDR,UL V1,UL V2);
UL RET(UL V1,UL V2,UL V3);
UL PUSH(UL REG0,UL V1,UL V2);
UL POP(UL REG0,UL V1,UL V2);
UL LOADB(UL REG0,UL ADDR,UL V1);
UL LOADW(UL REG0,UL ADDR,UL V1);
UL STOREB(UL REG0,UL ADDR,UL V1);
UL STOREW(UL REG0,UL ADDR,UL V1);
UL LOADI(UL REG0,UL IMM,UL V1);
UL NOP(UL V1,UL V2,UL V3);
UL IN(UL REG0,UL PORT,UL V1);
UL OUT(UL REG0,UL PORT,UL V1);
UL ADD(UL REG0,UL REG1,UL REG2);
UL ADDI(UL REG0,UL IMM,UL V1);
UL SUB(UL REG0,UL REG1,UL REG2);
UL SUBI(UL REG0,UL IMM,UL V1);
UL MUL(UL REG0,UL REG1,UL REG2);
UL DIV(UL REG0,UL REG1,UL REG2);
UL AND(UL REG0,UL REG1,UL REG2);
UL OR(UL REG0,UL REG1,UL REG2);
UL NOR(UL REG0,UL REG1,UL REG2);
UL NOTB(UL REG0,UL REG1,UL V1);
UL SAL(UL REG0,UL REG1,UL REG2);
UL SAR(UL REG0,UL REG1,UL REG2);
UL EQU(UL REG0,UL REG1,UL V1);
UL LT(UL REG0,UL REG1,UL V1);
UL LTE(UL REG0,UL REG1,UL V1);
UL NOTC(UL V1,UL V2,UL V3);

struct data_token{
    char  t_name[15];
    unsigned long   byte_pos;
    unsigned long   size;
    //value_buf used to store initialized value
    unsigned char *value_buf;
    struct data_token *next;
}data_head;

struct pos_token{
    char  t_name[15];
    unsigned long  lines;
    struct pos_token *next;
}pos_head;

#endif // joj
