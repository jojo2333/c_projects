//coding=utf-8
/*
    compiler main program
    created at:2016-07-08
    created by�����峽
*/
#include "defines.h"

UL (*ops[])(UL,UL,UL) = {HLT,JMP,CJMP,OJMP,CALL,RET,PUSH,POP,
                      LOADB,LOADW,STOREB,STOREW,LOADI,NOP,IN,OUT,
                      ADD,ADDI,SUB,SUBI,MUL,DIV,AND,OR,
                      NOR,NOTB,SAL,SAR,EQU,LT,LTE,NOTC};

char OPS[][8] = {"HLT","JMP","CJMP","OJMP","CALL","RET","PUSH","POP",
              "LOADB","LOADW","STOREB","STOREW","LOADI","NOP","IN","OUT",
              "ADD","ADDI","SUB","SUBI","MUL","DIV","AND","OR",
              "NOR","NOTB","SAL","SAR","EQU","LT","LTE","NOTC"};
unsigned long total_commands = 0;
//define easy way to find a certain character
char * find(char *p,char c)
{
    while(*p)
    {
        if(*p == c)
            return p;
        p++;
    }
    return NULL;
}
//define a easy way to find the corresponding function
int find_op(char *p)
{
    int i;
    for(i=0;i<32;i++)
        if(!strcmp(p,OPS[i]))
        return i;
    return -1;
}



void pre_process(char *file)
{
    FILE *fpi = fopen(file,"r");
    //using temperery file
    FILE *fpo = fopen("temp","w");
    char a[200];
    int line = 1;
    struct data_token *d_current = &data_head;
    struct pos_token  *p_current = &pos_head;
    d_current->byte_pos = 0;
    d_current->next = NULL;
    p_current->next = NULL;

    while(fgets(a,200,fpi)!=NULL)
    {
        char op[10];
        //find'#' and replace it with '\0'
        if(find(a,'#')!=NULL)
        {
            char *p = find(a,'#');
            *p++ = '\n'; *p = '\0';
        }
        //if met with blank line continue to next line
        if(sscanf(a,"%s",op)==-1)            continue;
        //find if the line begins with "WORD" or "BYTE"
        if(strcmp(op,"WORD")==0||strcmp(op,"BYTE")==0)
        {
            d_current->next = malloc(sizeof(struct data_token));

            char token[15],*p;
            int  tsize=1;
            sscanf(a,"%*s%s",token);
            //if met with '[' treat the definition as an array
            p = find(token,'[');
            if(p!=NULL)
            {
                sscanf(p,"[%d]",&tsize);
                *p = '\0';
            }
            p = find(token,'=');
            if(p!=NULL)
            {
                *p = '\0';
            }

            if(strcmp(op,"WORD")==0)    d_current->size = tsize*2;
            else                        d_current->size = tsize;
            d_current->value_buf = malloc(sizeof(unsigned char)*d_current->size+1);
/*initialize value*/
            unsigned char *buf = d_current->value_buf;
            //judge whether the variable is initialized
            char *pnt;
            if((pnt=find(a,'='))!=NULL)
            {
                //if not attribute read the number and initialize it
                if(tsize==1)
                {
                    if(find(pnt,'\'')!=NULL)
                        sscanf(find(pnt,'\''),"'%c",(char *)buf);
                    else
                        sscanf(find(a,'='),"=%hu",(unsigned short*)buf);
                }
                else if((pnt=find(pnt,'\"'))!=NULL)
                {
                    pnt = pnt+1;
                    sscanf(pnt,"%s\"",(char *)buf);
                }
                //if is attribute find each value and initialize it
                else
                {
                    pnt = find(a,'{')+1;
                    sscanf(pnt,"%hu",(unsigned short*)buf);
                    if(strcmp(op,"WORD")==0)    buf = buf+2;
                    else                        buf = buf+1;
                    while((pnt=find(pnt,','))!=NULL)
                    {
                        pnt=pnt+1;
                        sscanf(pnt,"%hu",(unsigned short*)buf);
                        if(strcmp(op,"WORD")==0)    buf = buf+2;
                        else                        buf = buf+1;
                    }
                    if(d_current->size*sizeof(unsigned char)-(buf-d_current->value_buf)!=0)
                    memset(buf,0,d_current->size*sizeof(unsigned char)-(buf-d_current->value_buf));
                }
            }
            //if data not given set its value as 0
            else    memset(d_current->value_buf,0,sizeof(unsigned char)*d_current->size);
/*creat next node*/
            strcpy(d_current->t_name,token);
            d_current->next->byte_pos = d_current->byte_pos+d_current->size;
            d_current = d_current->next;
            d_current->next = NULL;
            continue;
        }
        // search for the end of jump token
        else if(find(op,':')!=NULL)
        {
            *(find(op,':')) = '\0';
            //record the name and destine line of the jump token
            p_current->lines = line;
            strcpy(p_current->t_name,op);
            p_current->next = malloc(sizeof(struct pos_token));
            p_current = p_current->next;
            p_current->next = NULL;

            char *s,*p = find(a,':');
            for(s=a;s<=p;s++)
                *s = ' ';
            total_commands++;
        }
        else
            total_commands++;
        line++;
        fprintf(fpo,"%s",a);
    }
    fclose(fpi);
    fclose(fpo);
}





int main(int argc,char **argv)
{
    pre_process(argv[1]);
    FILE *fpi=fopen("temp","r");
    FILE *fpo=fopen("result.txt","w");

    char cache[100];
    char op[10],sy1[10],sy2[10],sy3[10];
    fprintf(fpo,"0x%08lx\n",total_commands);

    while(fgets(cache,100,fpi)!=NULL)
    {
        //record total amount of commands

        sscanf(cache,"%s%s%s%s",op,sy1,sy2,sy3);
        int func = find_op(op);
        unsigned long para1,para2,para3;
        // if it is to jump
        if(strlen(sy1)!=1||find("ABCDEFGZ",*sy1)==NULL)             //CALL JUMP..
        {
            struct pos_token  *p_current = &pos_head;
            while(p_current->next!=NULL)
            {
                if(!strcmp(p_current->t_name,sy1))
                {
                    para1 = (p_current->lines-1)*4;
                    break;
                }
                p_current = p_current->next;
            }
        }
        else
        // get the value of the first parameter
        {
            if(*sy1 == 'Z')   para1 = 0;
            else            para1 = *sy1 -'A'+1;
        }

        //judging it is an variable or it is a symbol of register
        if(strlen(sy1)!=1||find("ABCDEFGZ",*sy2)==NULL)
        {
            if(isdigit(*sy2)||*sy2=='-')
            {
                short temp = atoi(sy2);
                para2 = 0xffff&temp;
            }
            else
            {
                struct data_token *d_current = &data_head;
                while(d_current->next!=NULL)
                {
                    if(!strcmp(d_current->t_name,sy2))
                    {
                        para2 = d_current->byte_pos;
                        break;
                    }
                    d_current = d_current->next;
                }
            }
        }
        //get the value of the second parameter
        else
        {
            if(*sy2 == 'Z')   para2 = 0;
            else            para2 = *sy2 -'A'+1;
        }
        //get the value of the third parameter
        if(*sy3 == 'Z')   para3 = 0;
            else            para3 = *sy3 -'A'+1;

        unsigned long command = ops[func](para1,para2,para3);
        fprintf(fpo,"0x%08lx\n",command);
    }

    // at the end of output code record the initialized data
    struct data_token *d_current = &data_head;
    int i=0;
    unsigned long total_size=0,val=0;

    //record the amount of command into machine code
    while(d_current->next!=NULL)
    {
        int j;
        unsigned char *buf = d_current->value_buf;
        total_size +=d_current->size;
        for(j=0;j<d_current->size;j++)
        {
            val |= (*(buf+j)<<i*8);
            i++;
            if(i==4)
            {
                fprintf(fpo,"0x%08lx\n",val);
                i=0;
                val=0;
            }
        }
        d_current = d_current->next;
    }
    if(i!=0)
        fprintf(fpo,"0x%08lx\n",val);
    //fprintf(fpo,"0x%08lx\n",total_size);

    fclose(fpi);
    fclose(fpo);

    return 0;
}
